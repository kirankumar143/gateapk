import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
// import { Login1Page } from '../pages/login1/login1';
import { GatehomePage } from '../pages/gatehome/gatehome';
import { GateformPage } from '../pages/gateform/gateform';
import { GatevisitorsPage } from '../pages/gatevisitors/gatevisitors';
import { VisitorlistPage } from '../pages/visitorlist/visitorlist';
import { ApiProvider } from '../providers/api/api';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AddlistvisitortabPage } from '../pages/addlistvisitortab/addlistvisitortab';
import { HttpModule } from '@angular/http';
import { MaidlistPage } from '../pages/maidlist/maidlist';
import { AddlistmaidtabPage } from '../pages/addlistmaidtab/addlistmaidtab';
import { DriverlistPage } from '../pages/driverlist/driverlist';
import { AdddriverPage } from '../pages/adddriver/adddriver';
import { DrivertabPage } from '../pages/drivertab/drivertab';
import { DeliveryboylistPage } from '../pages/deliveryboylist/deliveryboylist';
import { AdddeliveryboyPage } from '../pages/adddeliveryboy/adddeliveryboy';
import { DeliveryboytabPage } from '../pages/deliveryboytab/deliveryboytab';
import { Camera } from '@ionic-native/camera';
import { MomentModule } from 'angular2-moment';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { HistorytabPage } from '../pages/historytab/historytab';
import { HactivelistPage } from '../pages/hactivelist/hactivelist';
import { H48hrslistPage } from '../pages/h48hrslist/h48hrslist';
import { OtherslistPage } from '../pages/otherslist/otherslist';
import { OtherstabPage } from '../pages/otherstab/otherstab';
import { AddothersPage } from '../pages/addothers/addothers';
import { BusPage } from '../pages/bus/bus';
import { AddbusPage } from '../pages/addbus/addbus';
import { BusdetailsPage } from '../pages/busdetails/busdetails';
import { VisdetailsPage } from '../pages/visdetails/visdetails';
import { StaffsPage } from '../pages/staffs/staffs';
import { StaffpopoverComponent } from '../components/staffpopover/staffpopover';
import { StaffloginpopoverComponent } from '../components/staffloginpopover/staffloginpopover';
import { StaffhistoryPage } from '../pages/staffhistory/staffhistory';
import { AddguestPage } from '../pages/addguest/addguest';
import { GuestlistPage } from '../pages/guestlist/guestlist';
import { GuesttabPage } from '../pages/guesttab/guesttab';
import { ExpVisPage } from '../pages/exp-vis/exp-vis';
import { ExpvisLoginPopupComponent } from '../components/expvis-login-popup/expvis-login-popup';
import { KeepatgatelistPage } from '../pages/keepatgatelist/keepatgatelist';
import { KeepatgatetabPage } from '../pages/keepatgatetab/keepatgatetab';
import { IssueddellistPage } from '../pages/issueddellist/issueddellist';
import { KagPopoverComponent } from '../components/kag-popover/kag-popover';
import { Autostart } from '@ionic-native/autostart';
import { ParkingSlotsPage } from '../pages/parking-slots/parking-slots';
import { ViolationListPage } from '../pages/violation-list/violation-list';
import { AddViolationPage } from '../pages/add-violation/add-violation';
import { EditbusPage } from '../pages/editbus/editbus';
import { BusaddPage } from '../pages/busadd/busadd';
import { AddAllPage } from '../pages/add-all/add-all';
import { AllVisitorsListPage } from '../pages/all-visitors-list/all-visitors-list';
import { HomePage } from '../pages/home/home';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {IonMarqueeModule} from "ionic-marquee";
import { InsidePage } from '../pages/inside/inside';
import { WaitingPage } from '../pages/waiting/waiting';
import { ExpectedPage } from '../pages/expected/expected';
import { ViolationPage } from '../pages/violation/violation';
import { NotesPage } from '../pages/notes/notes';
import { VerificationpagePage } from '../pages/verificationpage/verificationpage';
import { SelectCompanyPage } from '../pages/select-company/select-company';
import { SelectPurposePage } from '../pages/select-purpose/select-purpose';
import { SelectFlatNumberPage } from '../pages/select-flat-number/select-flat-number';
import { AddNewStaffPage } from '../pages/add-new-staff/add-new-staff';
@NgModule({
  declarations: [
    MyApp,
    // Login1Page,
    GatehomePage,
    GateformPage,
    GatevisitorsPage,
    VisitorlistPage,
    AddlistvisitortabPage,
    MaidlistPage,
    AddlistmaidtabPage,
    DriverlistPage,
    AdddriverPage,
    DrivertabPage,
    DeliveryboylistPage,
    AdddeliveryboyPage,
    DeliveryboytabPage,
    HistorytabPage,
    HactivelistPage,
    H48hrslistPage,
    OtherstabPage,
    OtherslistPage,
    AddothersPage,
    BusPage,
    AddbusPage,
    BusdetailsPage,
    StaffsPage,
    VisdetailsPage,
    StaffpopoverComponent,
    StaffloginpopoverComponent,
    StaffhistoryPage,
    AddguestPage,
    GuestlistPage,
    GuesttabPage,
    ExpVisPage,
    ExpvisLoginPopupComponent,
    KeepatgatelistPage,
    KeepatgatetabPage,
    IssueddellistPage,
    ParkingSlotsPage,
    KagPopoverComponent,
    ViolationListPage,
    AddViolationPage,
    EditbusPage,
    BusaddPage,
    AddAllPage,
    AllVisitorsListPage,
    HomePage,
    InsidePage,
    WaitingPage,
    ExpectedPage,
    ViolationPage,
    NotesPage,
    VerificationpagePage,
    SelectCompanyPage,
    SelectPurposePage,
    SelectFlatNumberPage,
    AddNewStaffPage

  ],
  imports: [
    BrowserModule,
    IonicImageViewerModule,
    HttpModule,
    MomentModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    IonMarqueeModule,
   
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    // Login1Page,
    GatehomePage,
    GateformPage,
    GatevisitorsPage,
    VisitorlistPage,
    AddlistvisitortabPage,
    MaidlistPage,
    AddlistmaidtabPage,
    DriverlistPage,
    AdddriverPage,
    DrivertabPage,
    DeliveryboylistPage,
    AdddeliveryboyPage,
    DeliveryboytabPage,
    HistorytabPage,
    HactivelistPage,
    H48hrslistPage,
    OtherstabPage,
    OtherslistPage,
    AddothersPage,
    BusPage,
    AddbusPage,
    BusdetailsPage,
    StaffsPage,
    VisdetailsPage,
    StaffpopoverComponent,
    StaffloginpopoverComponent,
    StaffhistoryPage,
    AddguestPage,
    GuestlistPage,
    GuesttabPage,
    ExpVisPage,
    ExpvisLoginPopupComponent,
    KeepatgatelistPage,
    KeepatgatetabPage,
    IssueddellistPage,
    ParkingSlotsPage,
    KagPopoverComponent,
    ViolationListPage,
    AddViolationPage,
    EditbusPage,
    BusaddPage,
    AddAllPage,
    AllVisitorsListPage,
    HomePage,
    InsidePage,
    WaitingPage,
    ExpectedPage,
    ViolationPage,
    NotesPage,
    VerificationpagePage,
    SelectCompanyPage,
    SelectPurposePage,
    SelectFlatNumberPage,
    AddNewStaffPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    Autostart
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
