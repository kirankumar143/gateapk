import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { Login1Page } from '../pages/login1/login1';
import { GatehomePage } from '../pages/gatehome/gatehome';
import { AddlistvisitortabPage } from '../pages/addlistvisitortab/addlistvisitortab';
import { AddlistmaidtabPage } from '../pages/addlistmaidtab/addlistmaidtab';
import { DrivertabPage } from '../pages/drivertab/drivertab';
import { DeliveryboytabPage } from '../pages/deliveryboytab/deliveryboytab';
import { Storage } from '@ionic/storage';
import { HistorytabPage } from '../pages/historytab/historytab';
import { OtherstabPage } from '../pages/otherstab/otherstab';
import { StaffsPage } from '../pages/staffs/staffs';
import { GuesttabPage } from '../pages/guesttab/guesttab';
import { BusPage } from '../pages/bus/bus';
import { ExpVisPage } from '../pages/exp-vis/exp-vis';
import { timer } from 'rxjs/observable/timer';
import { Autostart } from '@ionic-native/autostart';
import { HomePage } from '../pages/home/home';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  user: {};
  rootPage: any = HomePage;
  pages: Array<{ title: string, icon: string, component: any }>;
  showSplash = true;

  constructor(public autoStart: Autostart ,public store: Storage, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
    this.menuOpened();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'MyHome', icon: 'home', component: HomePage },
      { title: 'Home', icon: 'home', component: GatehomePage },
      { title: 'Maids', icon: 'ios-woman', component: AddlistmaidtabPage },
      { title: 'Visitors', icon: 'ios-person', component: AddlistvisitortabPage },
      { title: 'Drivers', icon: 'ios-car', component: DrivertabPage },
      { title: 'DeliveryBoys', icon: 'bicycle', component: DeliveryboytabPage },
      { title: 'Bus/Cab', icon: 'ios-bus', component: BusPage },
      { title: 'Staff', icon: 'ios-people', component: StaffsPage },
      { title: 'Guest', icon: 'contacts', component: GuesttabPage },
      { title: 'Expected Visitors', icon: 'ios-person', component: ExpVisPage },
      { title: 'Others', icon: 'ios-people', component: OtherstabPage },
      { title: 'History', icon: 'filing', component: HistorytabPage },
      // { title: 'Logout', icon: 'log-out', component: Login1Page },
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.autoStart.enable();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      timer(3000).subscribe(()=> this.showSplash = false)
    });
  }
  hideSplashScreen() {
    if (this.splashScreen) {
      setTimeout(() => {
        this.splashScreen.hide();
      }, 100);
    }
  }
  menuOpened() {
    this.store.get('user').then((res) => {
      this.user = res;
      console.log(this.user);
    });
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.title === 'Logout') {
      this.store.remove('user');
    }
    this.nav.setRoot(page.component);
  }
}
