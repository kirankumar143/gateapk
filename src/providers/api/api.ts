import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';


/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class ApiProvider {

  //apiUrl = 'http://13.233.126.181/paradigmdev/v0';
  //apiUrl = 'https://www.littleoffice.co.in/paradigmapi/v0';
  //apiUrl = 'https://www.littleoffice.co.in/paradigmdev/v0';
  //apiUrl = 'https://www.littleoffice.co.in/paradigmdev/v0';
  apiUrl = 'https://www.littleoffice.co.in:8443/paradigmdev/v0';
 //apiUrl = 'https://www.littleoffice.co.in:8443/paradigm1/v0';

  constructor(public http: Http) {
  }

  // GATE LOGIN API

//   doLogin(password: string) {
//     return this.http.get(this.apiUrl + '/user/loginpin?pin=' + password + '&usertype=gate').toPromise();
//   }

//   // Flat Details API

//   getFlatDetails(blockId: string, apartmentId: string,userId: string) {
//     return this.http.get(this.apiUrl + '/flat/occupiedflatlist?blockId=' + blockId + '&apartmentId=' + apartmentId+'&userId=' +userId).toPromise();
//   }
//   getFlatList(blockId: string, apartmentId: string, userId: string) {
//     return this.http.get(this.apiUrl + '/flat/flatlist?blockId=' + blockId + '&apartmentId=' + apartmentId + '&userId=' + userId).toPromise();
//   }
//   // Block Details API

//   getBlockDetails(apartmentId: string,userId: string) {
//     return this.http.get(this.apiUrl + '/block/getblocksbyapId?apartmentId=' + apartmentId+'&userId=' +userId).toPromise();
//   }

//   // Company API

//   getCompanyDetails(userId: string) {
//     return this.http.get(this.apiUrl + '/company/list?userId=' +userId).toPromise();
//   }

//   triggerOTP(mobile,userId: string) {
//     return this.http.get(this.apiUrl + '/verifyotp?phoneNumber=' + mobile+'&userId=' +userId).toPromise();
//   }

//   addCompany(cname: string, ccat: string, clogo: string,userId: string) {
//     return this.http.get(this.apiUrl + '/company/addcompany?companyName=' + cname + '&companyCategory=' + ccat + '&logo=' + clogo+'&userId=' +userId).toPromise();
//   }

//   searchCompany(srcCompany: string,userId: string) {
//     return this.http.get(this.apiUrl + '/company/list?search=' + srcCompany+'&userId=' +userId).toPromise();
//   }

//   // ADD API's

//   doAddVisitor(name: string, phoneno: string, vehicleNo: string, companyname: string, blockno: string, blockLabel: string, apid: string,vId: string,typeofVistor: string,userId: string,body: any) {
//     console.log(name);
//     return this.http.post(this.apiUrl + '/vistor/addVistor?name=' + name + '&phoneNumber=' + phoneno + '&typeofVistor='+typeofVistor+ '&vehicleNo=' + vehicleNo+'&blockNumber=' + blockno + '&plotNumber=' + blockLabel + '&apartmentId=' + apid + '&photoUrl=null&access=pending&companyName=' + companyname+'&vistorId='+vId+'&userId=' +userId,body).toPromise();
//   }
//   // Delevery  List APIs
//   getKAGList(apId: string,userId: string,search:string){
//     return this.http.get(this.apiUrl+'/vistor/gatekeeptatgatelist?apartmentId='+apId+'&typeofVistor=delivery&userId=' +userId+'&search='+ search).toPromise();
//   }
//   getRecievedDeliveryList(apId: string,userId: string){
//     return this.http.get(this.apiUrl+'/vistor/deliveredgatekeeptatgatelist?apartmentId='+apId+'&typeofVistor=delivery&userId=' +userId).toPromise();
//   }
//   doRecievedDelivery(vid: string, rId: string, plotName: string,resName: string,userId: string) {
//     return this.http.get(this.apiUrl + '/vistor/updateVistor?id='+vid+'&access=delivered&typeofVistor=delivery&allowedBy='+resName+'&relationId='+rId+'&plotNumber='+plotName+'&userId=' +userId).toPromise();
//   }
//   // GET LIST API's

//   getVisitorList(apartmentId: string, lastId: string, srcVis: string, type: string,userId: string): Observable<any> {
//     return this.http.get(this.apiUrl + '/vistor/vistordetails?apartmentId=' + apartmentId + '&typeofVistor='+ type + '&page=10&lastId=' + lastId + '&search=' + srcVis+'&userId=' +userId);
//   }

//   getVisitorsList(apartmentId: string, lastId: string, srcVis: string, type: string,userId: string): Observable<any> {
//     return this.http.get(this.apiUrl + '/vistor/vistordetailsforgate?apartmentId=' + apartmentId + '&typeofVistor='+ type + '&page=10000&lastId=' + lastId + '&search=' + srcVis+'&userId=' +userId);
//   }

//   // http://localhost:8080/spr-mvc-hib/v0/vistor/gate/allvistordetailsforgate?apartmentId=72&userId=433
//   getAllVistorsList(apartmentId: string, lastId: string, srcVis: string,userId: string): Observable<any> {
//     return this.http.get(this.apiUrl + '/vistor/gate/allvistordetailsforgate?apartmentId=' + apartmentId +'&page=10000&lastId=' + lastId + '&search=' + srcVis+'&userId=' +userId);
//   }
//   getVisitorsPolling(apartmentId: string, lastId: string, srcVis: string, type: string, size: string,userId: string): Observable<any> {
//     return Observable.interval(5000).switchMap(() => this.http.get(this.apiUrl + '/vistor/vistordetailsgatepolling?apartmentId=' + apartmentId
//     + '&typeofVistor='+ type + '&page=10&lastId=' + lastId + '&search=' + srcVis+'&listSize='+ size+'&userId=' +userId)).map((res) =>{
//       const data =  JSON.parse(res['_body']);
//       if(data!=="" && data!==null && data!=="null"){
//         /* this.oldSize=data.totalSize; */
//         return data;
//       }
//     });
//   }
//   //).takeWhile(()=>this.start
//   //Gate Out API

//   onOut(visitorId: string, apId: string, flatName: string, relationId: string,userId: string) {
//     return this.http.get(this.apiUrl + '/vistor/out?vistorId=' + visitorId + '&apartmentId='+ apId + '&plotName=' + flatName +'&relationId='+relationId+'&userId=' +userId).toPromise();
//   }

//   // ALLOW VISITOR AND OTHERS API

//   allowVisitor(vid: string, rId: string, user: string, type: string,plotName: string,userId: string) {

//     return this.http.get(this.apiUrl + '/vistor/updateVistor?id=' + vid + '&access=allowed&typeofVistor='+type+'&allowedBy=' + user + ' Gaurd&relationId=' + rId +'&plotNumber='+plotName+'&userId=' +userId).toPromise();
//   }

//   // MAID AND DRIVER API's

//   doMaidLogin(maidPin: string, type: string,userId: string) {
//     return this.http.get(this.apiUrl + '/vistor/login?pinNo=' + maidPin + '&typeofVistor='+type+'&userId=' +userId).toPromise();
//   }

//   doMaidIn(maidId: string, blockNo: string, apId: string, type: string, flatName: string, asst: string,userId: string,body: any) {
//     return this.http.post(this.apiUrl + '/vistor/in?Id=' + maidId + '&typeofVistor='+type+'&apartmentId=' + apId +'&blockNumber='+blockNo+'&plotName=' + flatName +'&assets='+asst+'&userId=' +userId,body).toPromise();
//   }

//   // HISTORY API's

//   history48(apartmentId: string,userId: string) {
//     return this.http.get(this.apiUrl + '/vistor/vistorlisthistory48hours?apartmentId=' + apartmentId+'&userId=' +userId).toPromise();
//   }

//   historyActive(apartmentId: string,userId: string) {
//     return this.http.get(this.apiUrl + '/vistor/activevistorlist?apartmentId=' + apartmentId+'&userId=' +userId).toPromise();
//   }

//   // School Bus API's

//   addSchoolbus(name: string, phoneno: string, zone: string, agencyName: string, vehicleNo: string, blockno: string, blockLabel: string, apid: string,vId: string,userId: string,body: any): Observable<any> {
//     return this.http.post(this.apiUrl + '/vistor/addVistor?name=' + name + '&phoneNumber=' + phoneno + '&zone=' + zone + '&agencyName=' + agencyName + '&vehicleNo=' + vehicleNo + '&typeofVistor=bus&blockNumber=' + blockno + '&plotNumber=' + blockLabel + '&apartmentId=' + apid + '&photoUrl=null&access=pending&companyName=none'+'&vistorId='+vId+'&userId=' +userId,body);
//   }

//   getSchoolbusList(apartmentId: string,userId: string,search:string): Observable<any> {
//     return this.http.get(this.apiUrl + '/vistor/buslist?apartmentId=' + apartmentId + '&typeofVistor=bus&userId=' +userId +'&search='+search);
//   }

//   updateBusIn(bId: string, blockNo: string, apId: string,flatName: string,userId: string) {
//     return this.http.get(this.apiUrl + '/vistor/busarrive?Id=' + bId + '&typeofVistor=bus' + '&blockNumber=' + blockNo + '&access=arrive' + '&apartmentId=' + apId+'&plotName='+flatName+'&userId=' +userId).toPromise();
//   }
//   updateBus(bId:string, blockNo:string, flatId:string, userId:string ){
//     return this.http.get(this.apiUrl + '/vistor/editbus?id=' + bId +'&typeofVistor=bus' + '&blockNumber='+ blockNo + '&plotNumber='+flatId+'&userId='+userId).toPromise();
//   }
//   // OthersPage API's

//   addOthers(name: string, phoneno: string, vehicleNo: string, companyname: string, blockno: string, blockLabel: string, apid: string, profession: string,vId: string,userId: string,body: any) {
//     return this.http.post(this.apiUrl + '/vistor/addothers?name=' + name + '&phoneNumber=' + phoneno + '&vehicleNo=' +vehicleNo+ '&typeofVistor=others&companyName=' + companyname + '&blockNumber=' + blockno + '&plotNumber=' + blockLabel + '&apartmentId=' + apid + '&photoUrl=null&access=pending&profession=' + profession+'&vistorId='+vId+'&userId=' +userId,body).toPromise();
//   }

//   // Visitor History API

//   historyList(apartmentId: string, vistorId: string,userId: string) {
//     return this.http.get(this.apiUrl + '/vistorresidentrelation/relationhistorylistforgate?apartmentId=' + apartmentId + '&vistorId=' + vistorId+'&userId=' +userId).toPromise();
//   }


//   // STAFF API's

//   staffList(apartmentId: string,lastId: string, search: string,userId: string) {
//     return this.http.get(this.apiUrl + '/staff/stafflist?apartmentId=' + apartmentId+'&lastId='+lastId+'&page=10&search='+search+'&userId=' +userId).toPromise();
//   }
//   dayIn(staffId: string, allowedBy: string,userId: string) {
//     return this.http.get(this.apiUrl + '/staff/dayin?staffId=' + staffId + '&allowedBy=' + allowedBy+'&userId=' +userId).toPromise();
//   }
//   dayOut(rId :string,id :string,userId: string){
//     return this.http.get(this.apiUrl+'/staff/dayout?rId='+rId+'&id='+id+'&userId=' +userId) .toPromise();
//   }
//   addbreak(staffId: string, apartmentId: string, reason: string, staffRelationId: string,userId: string) {
//     return this.http.get(this.apiUrl + '/staffbreakrelation/startbreak?staffId=' + staffId + '&apartmentId=' + apartmentId + '&reason=' + reason + '&staffRelationId=' + staffRelationId+'&userId=' +userId).toPromise();
//   }
//   stopbreak(Id: string, staffId: string,userId: string) {
//     return this.http.get(this.apiUrl + '/staffbreakrelation/closebreak?id=' + Id+'&staffId='+staffId+'&userId=' +userId).toPromise();
//   }
//   staffRetireList(apartmentId: string,userId: string) {
//     return this.http.get(this.apiUrl + '/staff/staffretirelist?apartmentId=' + apartmentId+'&userId=' +userId).toPromise();
//   }
//   history( staffId: string, apartmentId: string, lastId: string,userId: string) {
//     return this.http.get(this.apiUrl + '/staffbreakrelation/staffdayhistory?staffId=' + staffId + '&apartmentId=' + apartmentId + '&lastId=' + lastId+'&userId=' +userId).toPromise();
//   }

//   // Guest Page API

//   addGuest(name: string, phoneno: string, blockno: string, blockLabel: string, apid: string, vId: string,userId: string,body: any): Observable<any> {
//     return this.http.post(this.apiUrl + '/vistor/addguest?name=' + name + '&phoneNumber=' + phoneno + '&typeofVistor=guest&blockNumber=' + blockno + '&plotNumber=' + blockLabel + '&apartmentId=' + apid+'&vistorId='+vId+'&userId=' +userId,body);
//   }

//   // Existing Visitor Details API

//   getExistVisDetails(phNo: string, type: string,userId: string) : Observable <any> {
//     return this.http.get(this.apiUrl+'/vistor/vlistbyphoneno?phoneNumber='+phNo+'&typeofVistor='+type+'&userId=' +userId);
//   }
//   getExistAllDetails(phNo:string,userId:string){
//     return this.http.get(this.apiUrl + '/vistor/listbyphoneno?phoneNumber='+phNo+'&userId='+userId).toPromise();
//   }
//   // Maid Assets API
//   addAssets(gold: string, cash: string, vId: string, other: string, mobile: string,userId: string)  {
//     return this.http.get(this.apiUrl+'/vistor/addassets?gold='+gold+'&cash='+cash+'&typeOfVistor=maid&vistorId='+vId+'&description='+other+'&mobile='+mobile+'&userId=' +userId).toPromise();
//   }

//   getAssetsList(assetId: string, visId: string,userId: string){
//     return this.http.get(this.apiUrl+'/vistor/assetslist?id='+ assetId +'&vistorId='+visId+'&userId=' +userId).toPromise();
//   }

//   // Expected Visitor List API
//   getExpVisList(apId: string,userId: string, search: string){
//     return this.http.get(this.apiUrl+'/vistor/expectedvistorlist?apartmentId='+ apId +'&typeofVistor=vistor&userId=' +userId+ '&search='+search).toPromise();
//   }

//   expVisLogin(pin: string,phoneNumber: string, name: string, apId: string, allowed: string,vehicleNo: string, blockNo: string,userId: string){
//     return this.http.get(this.apiUrl+'/vistor/expectedlogin?pinNo='+ pin +'&phoneNumber='+phoneNumber+'&name='+name+'&typeofVistor=vistor&apartmentId='+ apId +'&allowedBy='+allowed+'&vehicleNo='+vehicleNo+'&blockNumber='+blockNo+'&userId=' +userId).toPromise();
//   }
//   expVisOut(visId: string, apId: string, flatName: string,userId: string){
//     return this.http.get(this.apiUrl+'/vistor/out?vistorId='+ visId +'&apartmentId='+ apId +'&plotName='+flatName+'&userId=' +userId).toPromise();
//   }
//   // Parking slots list API
//   getParkingSlotList(apId: string,userId: string, search: string){
//     return this.http.get(this.apiUrl+'/flat/parkinglist?apartmentId='+ apId +'&userId='+ userId +'&search='+ search).toPromise();
//   }
//   // Violation API's
//   getViolationList(apId: string,page:string,lastId:string,srcViolation:string,userId: string){
//     return this.http.get(this.apiUrl+ '/flatviolation/approvedlistbyapartmentId?apartmentId='+apId+ '&page='+page+'&lastId='+lastId+'&search='+srcViolation+'&userId='+userId).toPromise();
//   }
//   getViolationsList(apId:string,userId:string){
//     return this.http.get(this.apiUrl+'/violation/list?apartmentId='+apId+'&userId='+userId).toPromise();
//   }
//   // Add Violation API's
//   addViolation(apId:string,bId:string,fId:string,fName:string,vId:string,penalty:string,raisedBy:string,remarks:string,vName:string,userId:string,body:any){
//     return this.http.post(this.apiUrl+ '/flatviolation/addflatviolation?apartmentId='+apId+'&blockId='+bId+'&flatId='+fId+'&flatName='+fName+'&violationId='+vId+'&penalty='+penalty+'&raisedBy='+raisedBy+'&remarks='+remarks+'&status=unapproved&violationName='+vName+'&userId='+userId,body).toPromise();
//   }
}

