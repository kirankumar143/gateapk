import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { BusPage } from '../bus/bus';
/**
 * Generated class for the BusaddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-busadd',
  templateUrl: 'busadd.html',
})
export class BusaddPage {
  user: {};
  blockList: any;
  flatList: any;
  block = '';
  flats = '';
  entity: any = [];
  value: any;
  required1 = false;
  required2 = false;
  search = '';
  busList: any;
  blockName: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public api: ApiProvider, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public viewCtrl: ViewController) {
  }
  ionViewDidLoad() {

    this.value = this.navParams.get('item');
    console.log(this.value);
    this.storage.get('user').then((val) => {
      this.user = (val);
      // this.api.getBlockDetails(this.user['apartmentId'], this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   console.log(data);
      //   this.blockList = data;
      // });
    })
  }
  onChangeBlock(block) {
    // this.api.getFlatList(block.id, this.user['apartmentId'], this.user['id']).then((res) => {
    //   const data = JSON.parse(res['_body']);
    //   console.log(data);
    //   this.flatList = data;
    // });
  }

  addBusFlat() {
    if (this.block == '') {
      return this.required1 = true;
    }
    if (this.flats == '') {
      return this.required2 = true;
    }
    let addBusLoader = this.loadingCtrl.create({
      content: 'adding flat'
    });
    addBusLoader.present();
    console.log(this.flats);
    // this.api.updateBus(this.value.id, this.value.blockNumber + ',' + this.block['id'] + "-" + this.flats['id'], this.value.plotNumber + ',' + this.block['name'] + "-" + this.flats['flatName'], this.user['id']).then((res) => {
    //   const data = JSON.parse(res['_body']);
    //   console.log(data);
    //   addBusLoader.dismiss();
    //   const toast = this.toastCtrl.create({
    //     message: 'New flat is added successfully.!',
    //     duration: 1000,
    //     position: 'bottom',
    //     cssClass: 'toast-green'
    //   });
    //   toast.present();
    //   this.navCtrl.push(BusPage);
    // })
  }
  onClickCancel() {
    this.viewCtrl.dismiss();
  }
}
