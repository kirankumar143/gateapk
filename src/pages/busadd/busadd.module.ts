import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusaddPage } from './busadd';

@NgModule({
  declarations: [
    BusaddPage,
  ],
  imports: [
    IonicPageModule.forChild(BusaddPage),
  ],
})
export class BusaddPageModule {}
