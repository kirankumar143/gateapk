import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditbusPage } from './editbus';

@NgModule({
  declarations: [
    EditbusPage,
  ],
  imports: [
    IonicPageModule.forChild(EditbusPage),
  ],
})
export class EditbusPageModule {}
