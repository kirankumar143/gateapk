import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { BusaddPage } from '../busadd/busadd';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the EditbusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editbus',
  templateUrl: 'editbus.html',
})
export class EditbusPage {
  entity: any;
  entity1 = '';
  entity2 = '';
  flats: any = [];
  blocks: any = [];
  posts: any;
  user: {};
  counts: number;
  constructor(public navCtrl: NavController, public storage: Storage, public navParams: NavParams, public api: ApiProvider, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {

  }
  ionViewWillEnter() {
    this.storage.get('user').then((val) => {
      this.user = (val);
      console.log(this.user);
    })
    this.entity = this.navParams.get('bus');
    console.log(this.entity);

    this.flats = this.entity.plotNumber.split(",");
    this.flats = this.flats.filter((item) => {
      return item != '';
    })
    this.counts = this.flats.length;

    this.blocks = this.entity.blockNumber.split(",");
    this.blocks = this.blocks.filter((item) => {
      return item != '';
    })
    this.counts = this.blocks.length;
  }
  onClickAddFlat() {
    this.navCtrl.push(BusaddPage, { item: this.entity });
  }
  deleteBus(bus, index) {

    let alert = this.alertCtrl.create({
      title: 'Are you sure ..!',
      subTitle: 'Do you want to delete this flat.?',
      buttons: [{
        text: 'Yes',
        handler: () => {
          let loading = this.loadingCtrl.create({
            content: 'deleting ...'
          });

          this.flats = this.flats.filter((item, i) => {
            return item != bus;
          })

          this.blocks = this.blocks.filter((item, i) => {
            return i != index;
          })

          console.log(this.flats, this.blocks);
          loading.present();
          // this.api.updateBus(this.entity['id'], this.blocks.join(), this.flats.join(), this.user['id']).then((res) => {
          //   const data = JSON.parse(res['_body']);
          //   console.log(data);
          //   loading.dismiss();
          //   this.navCtrl.pop();
          //   const toast = this.toastCtrl.create({
          //     message: 'Your flat is deleted successfully..!',
          //     duration: 3000,
          //     position: 'bottom',
          //     cssClass: 'toast-green'
          //   });
          //   toast.present()
          // });
        }
      },
      {
        text: 'Cancel'
      }]
    })
    alert.present();
  }
}
