import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParkingSlotsPage } from './parking-slots';

@NgModule({
  declarations: [
    ParkingSlotsPage,
  ],
  imports: [
    IonicPageModule.forChild(ParkingSlotsPage),
  ],
})
export class ParkingSlotsPageModule {}
