import { Component, ɵConsole } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { AddViolationPage } from '../add-violation/add-violation';

/**
 * Generated class for the ParkingSlotsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parking-slots',
  templateUrl: 'parking-slots.html',
})
export class ParkingSlotsPage {
  user: {};
  ParkingSlotList: any;
  search = "";
  isScroll = true;
  blockList : any;

  constructor(public navCtrl: NavController, public storage: Storage, public api: ApiProvider) {

  }
  ionViewDidLoad() {
    this.storage.get('user').then(val => {
      this.user = (val);
      // this.api.getParkingSlotList(this.user['apartmentId'], this.user['id'], this.search).then(res => {
      //   const data= JSON.parse(res['_body']);
      //   console.log(data);
      //   if(data==null){
      //     this.ParkingSlotList = 0;
      //   }else{
      //     this.ParkingSlotList = data;
      //   }
      // });
      // this.api.getBlockDetails(this.user['apartmentId'], this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   console.log(data);
      //   this.blockList = data;
      // });
    });
  }

  doRefresh(refresher){
    this.ionViewDidLoad();
    refresher.complete();
  }

  getClass(i) {
    if (i % 2 == 0) {
      return 'bg-green';
    }
    else if (i % 3 == 0) {
      return 'bg-red';
    }
    else if (i % 4 == 0) {
      return 'bg-blue';
    }
    else if (i % 5 == 0) {
      return 'bg-goldenrod';
    }
    else {
      return 'bg-purple';
    }
  }
  searchparkingslots() {
    this.storage.get('user').then(val => {
      this.user = (val);
      // this.api.getParkingSlotList(this.user['apartmentId'], this.user['id'], this.search).then(res => {
      //   const data = JSON.parse(res['_body']);
      //   this.ParkingSlotList = data;
      // })
    })
  }
  onClickAddViolation(item){
    this.navCtrl.push(AddViolationPage,{item:item});
    console.log( item.flatName);
  }
  /* doScrolling(infinite) {
    if (this.ParkingSlotList != null) {
      const tmp = this.ParkingSlotList[this.ParkingSlotList.length - 1];
      setTimeout(() => {
        this.isScroll = false;
        this.api.getParkingSlotList(this.user['apartmentId'], this.user['id'], this.search).then((res) => {
          const data = JSON.parse(res['_body']);
          if (data.list !== null) {
            this.ParkingSlotList = [...this.ParkingSlotList, ...data.list];
          }
          this.isScroll = true;
        });
      }, 1000);
    }
  } */
}
