import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddpicPage } from './addpic';

@NgModule({
  declarations: [
    AddpicPage,
  ],
  imports: [
    IonicPageModule.forChild(AddpicPage),
  ],
})
export class AddpicPageModule {}
