import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, AlertController, ToastController, Popover, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { StaffpopoverComponent } from '../../components/staffpopover/staffpopover';
import { StaffloginpopoverComponent } from '../../components/staffloginpopover/staffloginpopover';
import { StaffhistoryPage} from '../staffhistory/staffhistory';


/**
 * Generated class for the StaffsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-staffs',
  templateUrl: 'staffs.html',
})
export class StaffsPage {
  staff = 'Stafflist';
  user: string;
  reason: string;
  apartmentId: string;
  stafflist: any[] = [];
  rId = '';
  startbreak: Date;
  closebreak: Date;
  retiredlist: any[] = [];
  isScroll = true;
  currentPhoto: string;
  search = "";

  constructor(public toastCtrl: ToastController, public alertCtrl: AlertController, public popoverCtrl: PopoverController, public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public api: ApiProvider) {

  }

  ionViewWillEnter() {
    this.storage.get('user').then((val) => {
      this.user = (val);
      this.apartmentId = this.user["apartmentId"];
      // this.api.staffList(this.apartmentId, '10000', this.search,this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   console.log(data);
      //   this.stafflist = data;
      // });

      // this.api.staffRetireList(this.apartmentId,this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   this.retiredlist = data;
      // });
    });
  }

  doScrolling(infinite) {

    const tmp = this.stafflist[this.stafflist.length - 1];
    setTimeout(() => {
      this.isScroll = false;
      // this.api.staffList(this.apartmentId, tmp['id'], this.search,this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   console.log(data);
      //   if (data !== null) {
      //     this.stafflist = [...this.stafflist, ...data];
      //   }
      //   this.isScroll = true;
      // });
    }, 1000);
  }
  searchStaff() {
    // this.api.staffList(this.apartmentId, '10000', this.search,this.user['id']).then((res) => {
    //   const data = JSON.parse(res['_body']);
    //   this.stafflist = data;
    //   console.log(this.stafflist);
    // });
  }
  onClickLogout(item) {
      if(item.breakStatus != 'yes'){
        const confirm = this.alertCtrl.create({
          title: 'Are you sure..!',
          subTitle: 'to '+'<font color="red" size="4">LogOut</font> the entry of '+'<br><b>'+item.name+'</b>',
          buttons: [
            {
              text: 'Cancel',
              handler: () => {
              }
            },
            {
              text: 'Yes',
              handler: () => {
                // this.api.dayOut(item.rId, item.id,this.user['id']).then((res) => {
                //   item.access = 'left';
                //   item.outTime = new Date();
                // });
              }
            }
           ]
        });
        confirm.present();
      }else{
        const toast = this.toastCtrl.create({
          message: 'Please close break after do Logout',
          duration: 3000,
          position: 'bottom',
          cssClass: 'toast-orange'
        });
        toast.present();
      }
  }

  doRefresh(refresher) {
    this.ionViewWillEnter();
    refresher.complete();
  }

  presentPopover(event, item) {
    const popover: Popover = this.popoverCtrl.create(StaffpopoverComponent, { item: item});
    popover.present({
      ev: event
    });
    popover.onDidDismiss(data =>{
      this.ionViewWillEnter();
    })
  }

  prePopover(item) {
    const popover = this.popoverCtrl.create(StaffloginpopoverComponent, { item: item });
    popover.present();
  }
  onClickHistory(){
    this.navCtrl.push(StaffhistoryPage);
  }
}
