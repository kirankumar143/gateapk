import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { DeliveryboylistPage } from '../deliveryboylist/deliveryboylist';
import { AdddeliveryboyPage } from '../adddeliveryboy/adddeliveryboy';


/**
 * Generated class for the DeliveryboytabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-deliveryboytab',
  templateUrl: 'deliveryboytab.html',
})
export class DeliveryboytabPage {

  constructor(public storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
  }

  user: {};

  ionViewDidLoad() {
    this.storage.get('user').then((val) => {
    this.user = (val);
    });

  }

  tab1Root = AdddeliveryboyPage;
  tab2Root = DeliveryboylistPage;
}
