import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController ,AlertController, LoadingController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera'
import { ApiProvider } from '../../providers/api/api';
import { AllVisitorsListPage } from '../all-visitors-list/all-visitors-list';
/**
 * Generated class for the AdddeliveryboyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adddeliveryboy',
  templateUrl: 'adddeliveryboy.html',
})
export class AdddeliveryboyPage {

  mobileNo = "";
  mob = false;
  name = "";
  isname = false;
  companyName = "";
  vehicleNo: string;
  blockNo = "";
  flatNo = "";
  photoURL: string;
  next: number = 1;
  isDetails = true;
  isShowCompanies = false;
  selectedBlocks = [];
  isblock = false;
  user: {};
  selected: {};
  company: {};
  visitorPic = "";
  isvisitor = false;
  isShowPic = false;
  isEnabled = true;
  blocks = [];
  companies = [];
  iscompanies = false;
  flats = [];
  idPic = "";
  verify = false;
  isShowIdPic = false;
  catCompany: any[] = [];
  srcCompany: string;
  error: any;
  mobilePattern: string;
  otp: string;
  rotp: string;
  selectedBlockLabel: string;
  selectedBlockNo: string;
  selectedFlat = {};
  selectedBlock = {};
  wrong = false;
  visId = '';
  existVis: any=[];

  constructor(public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public api: ApiProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public camera: Camera,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {

    this.name = ""; this.mobileNo = ""; this.vehicleNo = ""; this.srcCompany = ""; this.selectedBlocks = []; this.flats = []; this.companyName = ""; this.visitorPic= ''; this.idPic='';
    this.selectedCompany = { id: 0, name: '' }; this.selectedBlock = {}; this.selectedFlat = {}; this.isShowIdPic = false; this.isShowPic = false;

    this.storage.get('user').then((val) => {
      this.user = (val);

      // this.api.getBlockDetails(this.user['apartmentId'],this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   this.blocks = data;
      //   console.log(this.blocks);
      // });

      // this.api.getCompanyDetails(this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   this.companies = data;
      //   console.log(this.companies);
      //   this.catCompany = this.getCompanes();
      //   console.log(this.catCompany);
      // })
    });

  }
  getCompanes() {
    let c = '';
    let cc: any[] = [];

    for (let i = 0; i < this.companies.length; i++) {
      if (c !== this.companies[i].companyCategory) {
        let obj = { id: this.companies[i].id, cat: this.companies[i].companyCategory, d: [this.companies[i]] };
        cc.push(obj);
        console.log(cc);
        c = this.companies[i].companyCategory;
      } else {
        cc[cc.length - 1].d.push(this.companies[i]);
      }
    }
    console.log(c);
    return cc;
  }

  e1: any = '';
  next1(e1) {
    e1.setFocus();
  }

  getPic() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 320,
      targetHeight: 320
    }).then((data) => {
      console.log(data);
      this.visitorPic = "data:image/jpeg;base64," + data;
      this.isShowPic = true;
    }, (error) => {

      console.log(error);
    });
  }
  getIdPic() {
    this.camera.getPicture({
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 600,
      targetHeight: 300
    }).then((data) => {
      console.log(data);
      this.idPic = "data:image/jpeg;base64," + data;
      this.isShowIdPic = true;
    }, (error) => {

    });
  }

  addVisitor() {
    if (!this.isShowPic) {
      const toast = this.toastCtrl.create({
        message: 'Add the photo',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return false;
    }
    let loading = this.loadingCtrl.create({
      content:'Processing ...'
    });
    loading.present();
    var body = {
      mainphoto: this.visitorPic,
      idcard: this.idPic,
      currentPhoto: null
    };
    // this.api.doAddVisitor(this.name, this.mobileNo,this.vehicleNo, this.companyName, this.selectedBlockNo, this.selectedBlockLabel, this.user['apartmentId'],this.visId,'delivery',this.user['id'],body).then((res) => {
    //     loading.dismiss();
    //   const toast = this.toastCtrl.create({
    //     message: 'New delivery is added successfully..!',
    //     duration: 3000,
    //     position: 'bottom',
    //     cssClass: 'toast-green'
    //   });
    //   toast.present();
    // }, (error) => {
    //   this.error = error;
    // });

    this.navCtrl.parent.select(1);
    this.next = 1;
    this.ionViewDidLoad();
  }

  searchComapny() {
    console.log(this.srcCompany);
    // this.api.searchCompany(this.srcCompany,this.user['id']).then((res) => {
    //   const data = JSON.parse(res['_body']);
    //   this.companies = data;
    //   console.log(this.companies);
    //   this.companyName = this.srcCompany;
    //   this.catCompany = this.getCompanes();
    //   console.log(this.catCompany);
    // })
  }
  addCompany() {
    // this.api.addCompany(this.companyName, 'Others', '',this.user['id']).then((res) => {
    //   console.log(res);
    // });
  }

  onClickBack() {
    if (this.next > 1) {
      this.next--;
    }
  }

  onClickNext() {
    if (this.next == 1) {
      if (this.mobileNo == "" || Number(this.mobileNo.length) < 10) {
        return this.mob = true;
      }
      else{
        // this.api.getExistVisDetails(this.mobileNo,'delivery',this.user['id']).subscribe((res)=>{
        //   const data = JSON.parse(res['_body']);
        //   this.existVis = data;
        //   console.log(this.existVis);
        //   if(this.existVis != undefined){
        //     if (this.existVis[0].access != 'left' && this.existVis[0].access != 'declined') {
        //       this.navCtrl.push(AllVisitorsListPage, { visitor: this.existVis });
        //     }
        //     this.visId = this.existVis[0].id;
        //     this.vehicleNo = this.existVis[0].vehicleNo;
        //     this.name = this.existVis[0].name;
        //     this.idPic = this.existVis[0].idCard;
        //     this.next = 4;
        //   }
        //   else{
        //       this.triggerOTP();
        //       this.next=2;
        //     }
        // });
       }
    }
    if (this.next == 2) {
      this.onClickOtpVerify();
      if (this.wrong) {
        return;
      }
    }
    if (this.next == 3) {
      if (this.name == "") {
        return this.isname = true;
      }else{
        this.next++;
      }
    }
    if (this.next == 5) {
      if (this.companies.length == 0) {
        const confirm = this.alertCtrl.create({
          title: 'New Company',
          subTitle: 'Name: ' + this.companyName,
          buttons: [
            {
              text: 'Add',
              handler: () => {
                console.log('Company added');
                // this.api.addCompany(this.companyName, 'Others', '',this.user['id']).then((res) => {
                //   this.next++;
                // });
              }
            }
          ]
        });

        confirm.present();
        return;
      }
    }
  }

  onClickAddNewCompany() {
    if (this.companies.length == 0) {
      const confirm = this.alertCtrl.create({
        title: 'New Company',
        subTitle: 'Name: ' + this.companyName,
        buttons: [
          {
            text: 'Add',
            handler: () => {
              console.log('Company added');
              // this.api.addCompany(this.companyName, 'Others', '',this.user['id']).then((res) => {
              //   this.next++;
              // });
            }
          }
        ]
      });
      confirm.present();
      return;
    }
  }

  triggerOTP() {
    // this.api.triggerOTP(this.mobileNo,this.user['id']).then((res) => {
    //   this.rotp = res['_body'];
    //   console.log(this.rotp);
    // });
  }

  selectedCompany: any = { id: 0, name: '' };
  onClickCompany(company) {
    if (this.companies.length > 0) {
      this.selectedCompany = company;
      this.companyName = company.companyName;
    }
  }

   onClickBlock(block) {
    let curApId = this.user['apartmentId'];
    this.selectedBlock = block;
    this.blockNo = block.name;
    console.log(this.selectedBlock);
    // this.api.getFlatDetails(this.selectedBlock['id'], curApId,this.user['id']).then((res) => {
    //   const data = JSON.parse(res['_body']);
    //   this.flats = data;
    // })

  }

  onClickFlat(flat) {
    let isflat = false;
    this.selectedFlat = flat;
    if(this.selectedBlocks.length <= 0){
      console.log(' first if statement');
      this.flatNo = flat.flatName;
      const obj = { block: this.selectedBlock, flat: this.selectedFlat };
      this.selectedBlocks.push(obj);
      this.displayFlat();
    }
    else{
      for(let i of this.selectedBlocks){
        console.log(i.flat.blockId, flat.blockId, i.flat.id, flat.id);
        if(flat.blockId == i.flat.blockId && flat.id == i.flat.id){
          console.log(' second if statement');
          isflat = true;
          break;
        }
      }
      if(!isflat){
        console.log(' third if statement');
        this.flatNo = flat.flatName;
          const obj = { block: this.selectedBlock, flat: this.selectedFlat };
          this.selectedBlocks.push(obj);
          this.displayFlat();
      }
    }
  }

  displayFlat() {
    let labelId = "";
    let labelName = "";
    if (this.selectedBlocks.length > 0) {
      for (let i = 0; i < this.selectedBlocks.length; i++) {
        if (i > 0) {
          labelName = `${labelName},`;
          labelId = `${labelId},`;
        }
        const curBlock = this.selectedBlocks[i].block;
        const curFlat = this.selectedBlocks[i].flat;
        labelId = labelId + curBlock.id + "-" + curFlat.id;
        labelName = labelName + curBlock.name + "-" + curFlat.flatName;
      }
    }
    console.log(labelId);
    this.selectedBlockNo = labelId;
    this.selectedBlockLabel = labelName;
  }

  onClickClose(idx) {
    this.selectedBlocks = this.selectedBlocks.filter((item, i) => {
      return idx !== i;
    });
    this.displayFlat();
  }

  onClickOtpVerify() {
    if (this.rotp === this.otp) {
      this.verify = true;
      this.wrong = false;
      this.otp = '';
      this.next++;
    } else {
      this.wrong = true;
    }
    console.log(this.otp);
  }

  isCompany() {
    if (this.companyName == '') {
      this.iscompanies = true;
      const toast = this.toastCtrl.create({
        message: 'Please select any company.!',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    else {
      this.next++;
    }
  }

  isBlock() {
    if (this.blockNo == '' || this.flatNo == '') {
      this.isblock = true;
      const toast = this.toastCtrl.create({
        message: 'Please select  a flat.!',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    else {
      if(this.existVis != undefined){
        this.next = 7;
      }else{
        this.next++;
      }
    }
  }

  isVisitor() {
     if (!this.isShowIdPic) {
      const toast = this.toastCtrl.create({
        message: 'ID is required..',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    else {
      this.next++;
    }
  }

}
