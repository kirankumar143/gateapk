import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api'
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';

/**
 * Generated class for the AddbusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addbus',
  templateUrl: 'addbus.html',
})
export class AddbusPage {

  mobileNo = '';
  name = '';
  vehicleNo: string;
  blockNo: string;
  flatNo: string;
  zone: string;
  agencyName: string;
  photoURL: string;
  next: number=1;
  isDetails = true;
  selectedBlocks:any = [];
  user: {};
  selected: {};
  visitorPic: string ;
  isShowPic = false;
  isShowPerson = true;
  isEnabled = true;
  blocks = [];
  flats = [];
  verify=false;
  error: any;
  mobilePattern: string;
  selectedBlockLabel:string;
  selectedBlockNo: string;
  selectedFlat: any = {};
  wrong= false;
  isMobile = false;
  isname = false;
  srcVis = "";
  visId='';
  isblock = false;

  constructor(public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public api: ApiProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public camera: Camera,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    this.storage.get('user').then((val) =>{
      this.user = (val);

      // this.api.getBlockDetails(this.user['apartmentId'],this.user['id']).then((res) =>{
      //   const data = JSON.parse(res['_body']);
      //   this.blocks = data;
      // });
    });

  }

  getPic() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 320,
      targetHeight: 320
    }).then((data) => {
     this.visitorPic = "data:image/jpeg;base64," + data;
     this.isShowPic = true;
     this.isShowPerson = false;

     }, (error) => {

  });
  }

  addSchoolbus() {
    if(!this.isShowPic){
      const toast = this.toastCtrl.create({
        message: 'Add bus photo',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return false;
    }
    let loading = this.loadingCtrl.create({
      content:'Processing ...'
    });
    loading.present();
    var body = {
      mainphoto: this.visitorPic,
      currentPhoto: null
    };
    // this.api.addSchoolbus(this.name, this.mobileNo,this.zone,this.agencyName,this.vehicleNo,this.selectedBlockNo,this.selectedBlockLabel,this.user['apartmentId'],this.visId,this.user['id'],body).subscribe((res) => {
    //   loading.dismiss();
    //   const toast = this.toastCtrl.create({
    //     message: 'New bus is added successfully.!',
    //     duration: 3000,
    //     position: 'bottom',
    //     cssClass: 'toast-green'
    //   });
    //   toast.present();
    // }, (error) => {
    //   this.error=error;
    // });
    this.navCtrl.pop();
    this.next=1;
    this.name=""; this.mobileNo=""; this.vehicleNo=""; this.selectedBlocks="";
  }

  onClickBack(){
    if(this.next>1) {
      this.next--;
    }

  }
  onClickNext(){

    if(this.next==1){
      //debugger;
      if(this.mobileNo=="" || Number(this.mobileNo.length) < 10){
        return this.isMobile = true;
      }
      if(this.name.length <= 3){
        return this.isname = true;
      }
    }
    this.next++;
  }

  isBlock() {
    if (this.blockNo == undefined || this.flatNo == undefined) {
      this.isblock = true;
      const toast = this.toastCtrl.create({
        message: 'Please select a flat',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    else {
      this.next++;
    }
  }


selectedBlock:any = {}
onClickBlock(block){
  let curApId = this.user['apartmentId'];
  this.selectedBlock = block;
  this.blockNo = block.name;
  // this.api.getFlatDetails(this.selectedBlock['id'],curApId,this.user['id']).then((res)=>{
  //   const data = JSON.parse(res['_body']);
  //   this.flats = data;
  // })

}

onClickFlat(flat) {
  let isflat = false;
  this.selectedFlat = flat;
  if(this.selectedBlocks.length <= 0){
    this.flatNo = flat.flatName;
    const obj = { block: this.selectedBlock, flat: this.selectedFlat };
    this.selectedBlocks.push(obj);
    this.displayFlat();
  }
  else{
    for(let i of this.selectedBlocks){
      if(flat.blockId == i.flat.blockId && flat.id == i.flat.id){
        isflat = true;
        break;
      }
    }
    if(!isflat){
      this.flatNo = flat.flatName;
        const obj = { block: this.selectedBlock, flat: this.selectedFlat };
        this.selectedBlocks.push(obj);
        this.displayFlat();
    }
  }
}

displayFlat() {
  let labelId  = "";
  let labelName = "";
  if(this.selectedBlocks.length > 0 ) {
    for(let i=0;i<this.selectedBlocks.length; i++) {
      if(i>0) {
        labelName =`${labelName},`;
        labelId = `${labelId},`;
      }
      const curBlock = this.selectedBlocks[i].block;
      const curFlat = this.selectedBlocks[i].flat;
      labelId=labelId+curBlock.id+"-"+curFlat.id;
      labelName=labelName+curBlock.name+"-"+curFlat.flatName;
    }
  }
  this.selectedBlockNo = labelId;
  this.selectedBlockLabel=labelName;
  }

  onClickClose(idx){
    this.selectedBlocks = this.selectedBlocks.filter((item, i) => {
      return idx !== i;
    });
    this.displayFlat();
  }

}
