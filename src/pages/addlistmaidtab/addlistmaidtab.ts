import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { GateformPage } from '../gateform/gateform';
import { MaidlistPage } from '../maidlist/maidlist';
/**
 * Generated class for the AddlistmaidtabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addlistmaidtab',
  templateUrl: 'addlistmaidtab.html',
})
export class AddlistmaidtabPage {

  constructor(public storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
  }

  user: {};

  ionViewDidLoad() {
    this.storage.get('user').then((val) =>  {
      this.user = (val);
      });
  }

  tab1Root = GateformPage;
  tab2Root = MaidlistPage;

}
