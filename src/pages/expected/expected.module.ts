import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExpectedPage } from './expected';

@NgModule({
  declarations: [
    ExpectedPage,
  ],
  imports: [
    IonicPageModule.forChild(ExpectedPage),
  ],
})
export class ExpectedPageModule {}
