import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { VisdetailsPage } from '../visdetails/visdetails';

/**
 * Generated class for the GuestlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-guestlist',
  templateUrl: 'guestlist.html',
})
export class GuestlistPage {

  user: {};
  guestList: any;
  apartmentId: string;
  srcVis='';
  isScroll=true;
  page =false;
  wait: number = 1;
  isData = false;

  constructor(public alertCtrl: AlertController, public api: ApiProvider,public storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillEnter() {
    this.storage.get('user').then((val) => {
      this.user = (val);
      this.apartmentId = this.user["apartmentId"];
      // this.api.getVisitorsList(this.user["apartmentId"], '10000', this.srcVis,'guest',this.user['id']).subscribe((res) => {
      //   const data = JSON.parse(res['_body']);
      //   console.log(data);
      //   this.page = true;
      // if(data.list !== null){
      // this.guestList = data.list;
      // this.onWait();
      // this.page = false;
      // }
      // else{
      //   this.isData = true;
      //   this.page = false;
      // }
      // });
    });
  }

  doRefresh(refresher) {
    this.ionViewWillEnter();
    refresher.complete();
  }

  doScrolling(infinite) {
    const tmp = this.guestList[this.guestList.length - 1];
    setTimeout(() => {
      this.isScroll = false;
      // this.api.getVisitorsList(this.apartmentId, tmp['id'], this.srcVis,'guest',this.user['id']).subscribe((res) => {
      //   const data = JSON.parse(res['_body']);
      //   if (data.list !== null) {
      //     this.guestList = [...this.guestList, ...data.list];
      //   }
      //   this.isScroll = true;
      // });
    }, 1000);
  }
  searchVis() {
    // this.api.getVisitorsList(this.apartmentId, '10000', this.srcVis,'guest',this.user['id']).subscribe((res) => {
    //   const data = JSON.parse(res['_body']);
    //   this.guestList = data.list;
    // });
  }

  onClickAllow(item) {
    event.stopPropagation();
    const confirm = this.alertCtrl.create({
      title: 'Issueing In Gate Pass',
      subTitle: 'Name: ' + item.name,
      buttons: [
        {
          text: 'Issue In Pass',
          handler: () => {
            // this.api.allowVisitor(item.vistorId, item.rId, this.user['username'],'guest', item.plotName,this.user['id']).then(res =>{
            //   this.ionViewWillEnter();
            // });
          }
        }
      ]
    });

    confirm.present();
  }
  onClickOut(item) {
    /* let date = new Date(parseInt(item.inTime)).toISOString().substring(0,10);
    let time = new Date(parseInt(item.inTime)).toISOString().substring(11,19);
    let data = date+' '+time; */
    event.stopPropagation();
    const confirm = this.alertCtrl.create({
      title: 'Issueing Out Gate Pass',
      subTitle: 'Name: ' + item.name,
      buttons: [
        {
          text: 'Issue Out Pass',
          handler: () => {
            // this.api.onOut(item.vistorId, this.user['apartmentId'], item.plotName, item.rId,this.user['id']).then((res)=>{
            //   this.ionViewWillEnter();
            // });
          }
        }
      ]
    });

    confirm.present();
  }

  pushWait(data) {
    return data.map((d) => {
      return { ...d, wait: 1 }
    });
  }

  onWait() {
    setTimeout(() => {
      if (this.wait <= 3) {
        this.wait++;
        this.onWait();
      }
      return
    }, 7000);
  }

  onClickArrow(event, item){
    event.stopPropagation();
    item.id = item.vistorId;
    this.navCtrl.push(VisdetailsPage, { item: item });
  }
  getClass(i){
    if(i%2==0){
      return 'bg-green';
    }
    else if(i%3==0){
      return 'bg-red';
    }
    else if(i%4==0){
      return 'bg-blue';
    }
    else if(i%5==0){
      return 'bg-goldenrod';
    }
    else{
      return 'bg-purple';
    }
  }
}
