import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Camera } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import { MaidlistPage } from '../maidlist/maidlist';
/**
 * Generated class for the GateformPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gateform',
  templateUrl: 'gateform.html',
})
export class GateformPage {

  isLoginFail = false;
  maidLoginShow = true;
  submitButtonShow = false;
  maid: {};
  maidId: string;
  user = {};
  currentPhoto: string;
  maidPin: string;
  maidPhoto: string;
  isShowOldPic = true;
  isShowCurPic = false;
  selectedBlocks = [];
  isCheckAsset: number = 0;
  selFlat = [];
  isFltSel = false;
  properties: string = '';
  money: string = '';
  other: string = '';
  error:any;
  

  constructor(public loadingCtrl: LoadingController, public toastCtrl: ToastController, public camera: Camera, public api: ApiProvider, public navCtrl: NavController, public navParams: NavParams,public storage: Storage) {
    this.maid=this.navParams.get('maid');
    this.storage.get('user').then((val) =>{
      this.user = (val);
    });
  }

  ionViewDidLoad() {
    if(this.maid){
      if(this.maid['plotName']){
        this.blockSelect = [];
      let p = this.maid['plotName'];
        let b = this.maid['blockNumber'];
        let bl = b.split(',');
        let pll = p.split(',');
        this.selectedBlocks = p.split(',');
        for (var i = 0; i < this.selectedBlocks.length; i++) {
          if (bl[i] !== '') {
            this.blockSelect.push({ id: bl[i], name: pll[i], selected: false });
          }
        }
      }
    }
  }

  e1: any = '';
  blockSelect: any[] = [];
  next1(e1) {
    e1.setFocus();
  }

  onClickEnter() {
    let loading = this.loadingCtrl.create({
    });
    loading.present();
    // this.api.doMaidLogin(this.maidPin, 'maid',this.user['id']).then((res) => {
    //   let data = null;
    //   try {
    //     data = JSON.parse(res['_body']);
    //     console.log(data);
    //     this.maid = data;
    //     this.maidPhoto = this.maid["photoUrl"];
    //     if (data.id && this.maidPin != undefined) {
    //       this.isLoginFail = false;
    //       this.maidLoginShow = false;
    //       this.submitButtonShow = true;
    //       this.maidId = data.id;
    //     } else {
    //       this.isLoginFail = true;
    //       this.maidPin = '';
    //     }
    //     this.blockSelect = [];
    //     let p = this.maid['plotNumber'];
    //     let b = this.maid['blockNumber'];
    //     let bl = b.split(',');
    //     let pll = p.split(',');
    //     this.selectedBlocks = p.split(',');
    //     for (var i = 0; i < this.selectedBlocks.length; i++) {
    //       if (bl[i] !== '') {
    //         this.blockSelect.push({ id: bl[i], name: pll[i], selected: false });
    //       }
    //     }
    //   } catch (e) {
    //     this.isLoginFail = true;
    //     this.maidPin = '';
    //   }
    // }, (error) => {
    //   this.isLoginFail = true;
    //   this.maidPin = '';
    // });
    loading.dismiss();
  }
  assets = [{ id: 1, name: 'phone-portrait', item: 'Mobile-1', status: '0' }, { id: 2, name: 'aperture', item: 'Gold-1', status: '0' }, { id: 3, name: 'hammer', item: 'Tools-1', status: '0' }];
  onClickAssets(ast) {
    if (ast.status == '0') {
      ast.status = '1';
    }
    else {
      ast.status = '0';
    }
  }

  onClickSelFlat(b) {
    b.selected = !b.selected;
    for (let i = 0; i < this.blockSelect.length; i++) {
      if (this.blockSelect[i] == b) {
        this.selFlat.push(b);
        this.isFltSel = true;
      }
    }
  }
  onClickGetMaidPic() {
    this.isShowCurPic = true;
    this.isShowOldPic = false;
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 320,
      targetHeight: 320
    }).then((data) => {
      this.currentPhoto = "data:image/jpeg;base64," + data;

    }, (error) => {
    });
  }
  blockNumber = '';
  plotName = '';
  onClickSubmit() {
    for (let i = 0; i < this.selFlat.length; i++) {
      if (this.selFlat[i].selected == true) {
        this.plotName = this.plotName + this.selFlat[i].name + ",";
        this.blockNumber = this.blockNumber + this.selFlat[i].id + ",";
      }
    }
    if (this.blockNumber == '' || this.plotName == '') {
      const toast = this.toastCtrl.create({
        message: 'Plz select the Flat',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }

    if (this.currentPhoto == undefined) {
      const toast = this.toastCtrl.create({
        message: 'Plz add maid current photo',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    for (let ast of this.assets) {
      if (ast.status !== '0') {
        this.properties += ast.item + ',';
      }
    }
    if (this.money !== '') {
      this.properties += this.money + ',';
    }
    if (this.other !== '') {
      this.properties += this.other + ',';
    }
    let loading = this.loadingCtrl.create({
      content:'Processing ...'
    });
    loading.present();
    // this.api.doMaidIn(this.maid['id'], this.blockNumber, this.maid['apartmentId'], 'maid', this.plotName, this.properties, this.user['id'], this.currentPhoto).then((res) => {
    //   loading.dismiss();
    //   const toast = this.toastCtrl.create({
    //     message: 'Maid is added successfully..!',
    //     duration: 3000,
    //     position: 'bottom',
    //     cssClass: 'toast-green'
    //   });
    //   toast.present();
    // }, (error) => {
    //   this.error = error;
    //   loading.dismiss();
    // });
      this.maidLoginShow = true;
      this.submitButtonShow = false;
      this.maidPin = '';
      this.navCtrl.push(MaidlistPage);
    
  }
}
