import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectPurposePage } from './select-purpose';

@NgModule({
  declarations: [
    SelectPurposePage,
  ],
  imports: [
    IonicPageModule.forChild(SelectPurposePage),
  ],
})
export class SelectPurposePageModule {}
