import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { VerificationpagePage } from '../verificationpage/verificationpage';


@IonicPage()
@Component({
  selector: 'page-select-purpose',
  templateUrl: 'select-purpose.html',
  
})
export class SelectPurposePage {
  title: string;
  value: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,  
      public viewCtrl: ViewController
    ) {
  }
  public onClickCancel() {
    this.viewCtrl.dismiss();
  }
  public test(event ,item ){
    this.navCtrl.pop();
    }
dismissModal() {
   
  this.viewCtrl.dismiss(this.title);
  console.log(this.title);
}

}