import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the VisdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-visdetails',
  templateUrl: 'visdetails.html',
})
export class VisdetailsPage {
  user: {};
  historyList = [];
  isScroll = true;
  value: any;
  flatList: any;
  blockList: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public api: ApiProvider) {
    this.value = navParams.get('item');
    console.log(this.value);
  }

  ionViewDidLoad() {
    this.storage.get('user').then((val) => {
      this.user = (val);

      /* this.api.getBlockDetails(this.user['apartmentId']).then(block => {
        const data = JSON.parse(block['_body']);
        this.blockList = data;
      });

      this.api.getFlats(this.user['apartmentId']).then((flat) => {
        const data = JSON.parse(flat['_body']);
        this.flatList = data;
        this.flatList = this.flatList.map(flt => {
          for (let blk of this.blockList) {
            if (flt.blockId == blk.id) {
              flt.apartmentId = blk.name;
            }
          }
          return flt;
        });
      }); */

      // this.api.historyList(this.user['apartmentId'], this.value['id'],this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   this.historyList = data;
      //   console.log(this.historyList);
      //   /* this.historyList = this.historyList.map(resl => {
      //     for (let flat of this.flatList) {
      //       if (flat.blockId == resl.blockNo && flat.id == resl.flatNo) {
      //         resl.blockNo = flat.apartmentId;
      //         resl.flatNo = flat.flatName;
      //       }
      //     }
      //     return resl;
      //   }); */
      // });
    });
  }

}
