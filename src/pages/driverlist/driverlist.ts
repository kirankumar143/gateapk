import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams} from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { VisdetailsPage } from '../visdetails/visdetails';
/**
 * Generated class for the DriverlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-driverlist',
  templateUrl: 'driverlist.html',
})
export class DriverlistPage {

  user: {};
  apartmentId: string;
  driverList: any;
  driverId: string;
  driverStatus: string;
  srcVis = '';
  isScroll = true;
  page = false;
  isData = false;

  constructor(
    public alertCtrl: AlertController,
    public api: ApiProvider,
    public storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
  ) { }

  ionViewWillEnter() {
    this.storage.get('user').then((val) => {
      this.user = (val);
      this.apartmentId = this.user["apartmentId"];
      // this.api.getVisitorsList(this.apartmentId, '10000', this.srcVis, 'driver',this.user['id']).subscribe((res) => {
      //   const data = JSON.parse(res['_body']);
      //   console.log(data);
      //   this.page = true;
      //   if (data.list !== null) {
      //     this.driverList = data.list;
      //     this.page = false;
      //   }
      //   else {
      //     this.isData = true;
      //     this.page = false;
      //   }
      // });
    });
  }

  doRefresh(refresher) {
    this.ionViewWillEnter();
    refresher.complete();
  }

  doScrolling(infinite) {
    if(this.driverList!=null){
    const tmp = this.driverList[this.driverList.length - 1];
    setTimeout(() => {
      this.isScroll = false;
      // this.api.getVisitorsList(this.apartmentId, tmp['id'], this.srcVis, 'driver',this.user['id']).subscribe((res) => {
      //   const data = JSON.parse(res['_body']);
      //   if (data.list !== null) {
      //     this.driverList = [...this.driverList, ...data.list];
      //   }
      //   this.isScroll = true;
      // });
    }, 1000);
  }
  }
  searchVis() {
    // this.api.getVisitorsList(this.apartmentId, '10000', this.srcVis, 'driver',this.user['id']).subscribe((res) => {
    //   const data = JSON.parse(res['_body']);
    //   if(data.list==null){
    //     this.driverList = 0;
    //   }
    //   else{
    //     this.driverList = data.list;
    //   }
    // });
  }

  onClickOut(item) {
    event.stopPropagation();
    const confirm = this.alertCtrl.create({
      title: 'Issueing Out Gate Pass',
      message: 'Name: ' + item.name,
      buttons: [
        {
          text: 'Issue Out Pass',
          handler: () => {
            console.log('Disagree clicked');
            // this.api.onOut(item.vistorId, this.user['apartmentId'], item.plotName, item.rId,this.user['id']).then((res) => {
            //   this.ionViewWillEnter();
            //   this.navCtrl.pop();
            // });
          }
        }
      ]
    });
    confirm.present();
  }

  onClickArrow(event, item) {
    event.stopPropagation();
    item.id = item.vistorId;
    this.navCtrl.push(VisdetailsPage, { item: item });
  }

}
