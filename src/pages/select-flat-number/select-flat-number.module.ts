import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectFlatNumberPage } from './select-flat-number';

@NgModule({
  declarations: [
    SelectFlatNumberPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectFlatNumberPage),
  ],
})
export class SelectFlatNumberPageModule {}
