import { Component,Input  } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController,ViewController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { VerificationpagePage } from '../verificationpage/verificationpage';

@IonicPage()
@Component({
  selector: 'page-select-flat-number',
  templateUrl: 'select-flat-number.html',
})
export class SelectFlatNumberPage {
  selectedBlocks = [];
  selectedBlockNo: string;
  selectedBlockLabel: string;
  next: number = 1;
  companies = [];
  companyName = "";
  user: {};
  selectedBlock = {};
  blockNo = "";
  flats = [];
  selectedFlat = {};
  flatNo = "";
  isblock = false;
  existVis: any;
  blocks = [];


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public api: ApiProvider,
    public toastCtrl: ToastController,
    public storage: Storage,
    public viewCtrl: ViewController

  ) {


  }
  dismissModal() {
   
    this.viewCtrl.dismiss(this.selectedBlocks);
    console.log(this.selectedBlocks);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectFlatNumberPage');
    this.storage.get('user').then((val) => {
      this.user = (val);
      // this.api.getBlockDetails(this.user['apartmentId'], this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   this.blocks = data;
      // });

    });
  }
 
  onClickClose(idx) {
      this.selectedBlocks = this.selectedBlocks.filter((item, i) => {
        return idx !== i;
      });
      this.displayFlat();
    }
  onClickBlock(block) {
      let curApId = this.user['apartmentId'];
      this.selectedBlock = block;
      this.blockNo = block.name;
      // this.api.getFlatDetails(this.selectedBlock['id'], curApId, this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   console.log(data);
      //   this.flats = data;
      // });
    }
    public onClickCancel() {
      this.viewCtrl.dismiss();
    }
  onClickFlat(flat) {
      let isflat = false;
      this.selectedFlat = flat;
      if(this.selectedBlocks.length <= 0) { 
      this.flatNo = flat.flatName;
      const obj = { block: this.selectedBlock, flat: this.selectedFlat };
      this.selectedBlocks.push(obj);
      this.displayFlat();
    }
    else {
      for (let i of this.selectedBlocks) {
        if (flat.blockId == i.flat.blockId && flat.id == i.flat.id) {
          isflat = true;
          break;
        }
      }
      if (!isflat) {
        this.flatNo = flat.flatName;
        const obj = { block: this.selectedBlock, flat: this.selectedFlat };
        this.selectedBlocks.push(obj);
        this.displayFlat();
      }
    }
  }
  displayFlat() {
    let labelId = "";
    let labelName = "";
    if (this.selectedBlocks.length > 0) {
      for (let i = 0; i < this.selectedBlocks.length; i++) {
        if (i > 0) {
          labelName = `${labelName},`;
          labelId = `${labelId},`;
        }
        const curBlock = this.selectedBlocks[i].block;
        const curFlat = this.selectedBlocks[i].flat;
        labelId = labelId + curBlock.id + "-" + curFlat.id;
        labelName = labelName + curBlock.name + "-" + curFlat.flatName;
      }
    }
    this.selectedBlockNo = labelId;
    this.selectedBlockLabel = labelName;
  }
  
  isBlock() {
    if (this.blockNo == '' || this.flatNo == '') {
      this.isblock = true;
      const toast = this.toastCtrl.create({
        message: 'Please select  a flat.!',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    else {
      if (this.existVis != undefined) {
        this.next = 6;
      }
      else {
        this.next++;
      }
    }
  }
  
}
