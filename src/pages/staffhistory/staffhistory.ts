import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the StaffhistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-staffhistory',
  templateUrl: 'staffhistory.html',
})
export class StaffhistoryPage {
  staff: {};
  user: {};
  apartmentId: string;
  srId: string;
  stafflist: any []=[];
  isstaffday = false;
  staffdayhistory : any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public storage: Storage,public api:ApiProvider,public  alertCtrl: AlertController) {
    
  }

  ionViewDidLoad() {
    this.staff = this.navParams.get('item');
    this.srId = this.navParams.get('srId');
    console.log(this.staff);
    this.storage.get('user').then((val) => {
      this.user = (val);
    //   this.api.history(this.staff['id'],this.staff['apartmentId'],'10000',this.user['id']).then((res)=>{
    //     const data = JSON.parse(res['_body']);
    //     this.staffdayhistory = data;
    // });
  });
  }
  /*isStaffDay() {
    if(this.staffdayhistory == ""){
    let alert = this.alertCtrl.create({
      title: 'Break Times',
      subTitle: 'Staff not gone through any break...',
      buttons: ['Dismiss']
    });
    alert.present();
  }
  else{
    this.staffdayhistory;
  }
}*/
}
