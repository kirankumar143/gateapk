import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { DriverlistPage } from '../driverlist/driverlist';

/**
 * Generated class for the AdddriverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adddriver',
  templateUrl: 'adddriver.html',
})
export class AdddriverPage {
  isLoginFail = false;
  driverLoginShow = true;
  submitButtonShow = false;
  driver: {};
  driverId: string;
  currentPhoto: string;
  driverPin = "";
  user = {};
  driverPhoto: string;
  isShowCurPic = false;
  selectedBlocks = [];
  isCheckAsset: number = 0;
  selFlat = [];
  isFltSel = false;
  blockSelect = [];
  properties: string = '';
  money: string = '';
  other: string = '';
  error:any;
  constructor(
    public toastCtrl: ToastController,
    public camera: Camera,
    public api: ApiProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage:Storage,
    public loadingCtrl: LoadingController) {
      this.driver=this.navParams.get('driver');
      console.log(this.driver);
      this.storage.get('user').then(val =>{
        this.user = val;
      })
  }
  ionViewDidLoad() {
    if(this.driver){
      if(this.driver['plotName']){
        this.blockSelect = [];
        let p = this.driver['plotName'];
          let b = this.driver['blockNumber'];
          let bl = b.split(',');
          let pll = p.split(',');
          this.selectedBlocks = p.split(',');
          for (var i = 0; i < this.selectedBlocks.length; i++) {
            if (bl[i] !== '') {
              this.blockSelect.push({ id: bl[i], name: pll[i], selected: false });
            }
          }
      }
    }

  }

  e1: any = '';

  next1(e1) {
    e1.setFocus();
  }

  onClickEnter() {
    let loading=this.loadingCtrl.create({
    });
    
    if (this.driverPin == "") {
      return this.isLoginFail = true;
    }
    loading.present();
    if (this.driverPin) {
      // this.api.doMaidLogin(this.driverPin, 'driver',this.user['id']).then((res) => {
      //   let data = null;
      //   try {
      //     data = JSON.parse(res['_body']);
      //     console.log(data);
      //     this.driver = data;
      //     this.driverPhoto = this.driver["photoUrl"];
      //     if (data.id) {
      //       this.isLoginFail = false;
      //       this.driverLoginShow = false;
      //       this.submitButtonShow = true;
      //       this.driverId = data.id;
      //     } else {
      //       this.isLoginFail = true;
      //       this.driverPin = '';
      //     }
      //     this.blockSelect = [];
      //     let p = this.driver['plotNumber'];
      //     let b = this.driver['blockNumber'];
      //     let bl = b.split(',');
      //     let pll = p.split(',');
      //     this.selectedBlocks = p.split(',');
      //     for (var i = 0; i < this.selectedBlocks.length; i++) {
      //       if (bl[i] !== '') {
      //         this.blockSelect.push({ id: bl[i], name: pll[i], selected: false });
      //       }
      //     }
      //   } catch (e) {
      //     this.isLoginFail = true;
      //     this.driverPin = '';
      //   }
      // }, (error) => {
      //   this.isLoginFail = true;
      //   this.driverPin = '';
      // });
    }
    loading.dismiss();
  }

  assets = [{id:1, name:'../../assets/imgs/mobile.svg', item:'Mobile-1', name1:'phone',status: '0'},{id:2, name:'../../assets/imgs/necklace.svg', name1:'gold',item:'Gold-1', status: '0'},{id:3, name:'../../assets/imgs/handyman-tools.svg', item:'Tools-1',name1:'tools', status: '0'}];
  onClickAssets(ast) {
    if(ast.status=='0'){
      ast.status='1';
    }
    else{
      ast.status='0';
    }
  }

  onClickSelFlat(b) {
    b.selected = !b.selected;
    for (let i = 0; i < this.blockSelect.length; i++) {
      if (this.blockSelect[i] == b) {
        this.selFlat.push(b);
        this.isFltSel = true;
      }
    }
  }

  onClickGetDriverPic() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 320,
      targetHeight: 320
    }).then((data) => {
      this.currentPhoto = "data:image/jpeg;base64," + data;
      this.isShowCurPic = true;
    }, (error) => {

    });
  }
  blockNumber = '';
  plotName = '';
  onClickSubmit() {
    for (let i = 0; i < this.selFlat.length; i++) {
      if (this.selFlat[i].selected == true) {
        this.plotName = this.plotName + this.selFlat[i].name + ",";
        this.blockNumber = this.blockNumber + this.selFlat[i].id + ",";
      }
    }
    if (this.blockNumber == '' || this.plotName == '') {
      const toast = this.toastCtrl.create({
        message: 'Plz select the Flat',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    if (this.currentPhoto == undefined) {
      const toast = this.toastCtrl.create({
        message: 'Plz add driver current photo',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    for (let ast of this.assets) {
      if (ast.status !== '0') {
        this.properties += ast.item + ',';
      }
    }
    if (this.money !== '') {
      this.properties += this.money + ',';
    }
    if (this.other !== '') {
      this.properties += this.other + ',';
    }
    let loading = this.loadingCtrl.create({
      content:'Processing ...'
    });
    loading.present();
    // this.api.doMaidIn(this.driver['id'], this.driver['blockNumber'], this.driver['apartmentId'],'driver', this.plotName, this.properties, this.user['id'], this.currentPhoto).then((res) => {
    //   loading.dismiss();
    //   const toast = this.toastCtrl.create({
    //     message: 'Driver is added successfully..!',
    //     duration: 3000,
    //     position: 'bottom',
    //     cssClass: 'toast-green'
    //   });
    //   toast.present();
    // }, (error) => {
    //   this.error = error;
    //   loading.dismiss();
    // });
      this.driverLoginShow = true;
      this.submitButtonShow = false;
      this.driverPin = '';
      this.navCtrl.push(DriverlistPage);
  }
}
