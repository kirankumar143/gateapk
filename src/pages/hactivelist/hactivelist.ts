import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';
/**
 * Generated class for the HactivelistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hactivelist',
  templateUrl: 'hactivelist.html',
})
export class HactivelistPage {
  user: string;
  apartmentId: string;
  detailsList: any;
  page = false;

  constructor(public alertCtrl: AlertController, public storage: Storage, public api: ApiProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.storage.get('user').then((val) => {
      this.user = (val);
      this.apartmentId = this.user["apartmentId"];
      // this.api.historyActive(this.apartmentId,this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   if(data !== null){
      //     this.detailsList = data;
      //     this.page = false;
      //   }
      // });
    });
    if(this.detailsList == undefined) {
      this.page = !this.page;
    }
  }

  doRefresher(refresher){
    this.ionViewDidLoad();
    refresher.complete();
  }

  onClickMaidOut(item) {
    const confirm = this.alertCtrl.create({
      title: 'Issueing out gate pass',
      message: 'Name: ' + item.name,
      buttons: [
        {
          text: 'Issue Out Pass',
          handler: () => {
            // console.log('Disagree clicked');
            // this.api.onOut(item.id, this.user['apartmentId'], item.plotNumber, item.rId,this.user['id']).then((res)=>{
            // });
          }
        }
      ]
    });
    confirm.present();
  }
  onClickVisOut(item) {
      const confirm = this.alertCtrl.create({
        title: 'Issueing Out Gate Pass',
        subTitle: 'Name: '+item.name,
        buttons: [
          {
            text: 'Issue Out Pass',
            handler: () => {
              // this.api.onOut(item.id, this.user['apartmentId'], item.plotNumber, item.rId,this.user['id']).then((res)=>{
              // });
            }
          }
         ]
      });
      confirm.present();
     }

}
