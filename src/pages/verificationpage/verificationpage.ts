import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController, NavParams, ToastController, AlertController, LoadingController, ModalController, Modal } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Camera } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import { AllVisitorsListPage } from '../all-visitors-list/all-visitors-list';
import { GatehomePage } from '../gatehome/gatehome';
import { GateformPage } from '../gateform/gateform';
import { AdddriverPage } from '../adddriver/adddriver';
import { SelectCompanyPage } from '../select-company/select-company';
import { SelectFlatNumberPage } from '../select-flat-number/select-flat-number';
import { SelectPurposePage } from '../select-purpose/select-purpose';
import { AddpicPage } from '../addpic/addpic';
import { HomePage } from '../home/home';
import { SelectFlatNumberPageModule } from '../select-flat-number/select-flat-number.module';

@IonicPage()
@Component({
  selector: 'page-verificationpage',
  templateUrl: 'verificationpage.html',
})
export class VerificationpagePage {
  plotName: string;
  typeofVistor: string;
  mobileNo = "";
  mob = false;
  name = "";
  isname = false;
  companyName: string;
  vehicleNo: string;
  blockNo = "";
  flatNo = "";
  photoURL: string;
  next: number = 1;
  isDetails = true;
  isShowCompanies = false;
  selectedBlocks = [];
  isblock = false;
  user: {};
  selected: {};
  company: {};
  visitorPic = "";
  isvisitor = false;
  isShowPic = false;
  isEnabled = true;
  blocks = [];
  companies = [];
  iscompanies = false;
  flats = [];
  idPic = "";
  verify = false;
  isShowIdPic = false;
  catCompany: any[] = [];
  srcCompany: string;
  error: any;
  mobilePattern: string;
  otp: string;
  rotp: string;
  selectedBlockLabel: string;
  selectedBlockNo: string;
  selectedFlat = {};
  selectedBlock = {};
  wrong = false;
  visId = '';
  existVis: any;
  required = true;
  profession = "";
  isprofession = false;
  category: any = [];
  resCat = [];
  categories: any;
  cat_loop: number;
  title: string;
  userProfile: any;
  value: any;
  madalDismissData: any;
  selectedData: any;
  blockname: any;
  flatName: any;
  show: boolean;
  ishidden: boolean;
  sendingrequest: boolean;
  addpic: any;
  notshow: boolean;
  idcardimage =false;  
  idcardimagepic ="";


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public api: ApiProvider,
    public storage: Storage,
    public camera: Camera,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public modal: ModalController,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,

  ) {
    this.categories = [
      { cat: 'vistor', icon: '../../assets/imgs/visitor.png', style: '#5a056b' },
      { cat: 'delivery', icon: '../../assets/imgs/delivery.svg', style: '#ee4b19' },
      { cat: 'others', icon: '../../assets/imgs/others.png', style: '#f2994a' },
      { cat: 'guest', icon: '../../assets/imgs/Guests.png', style: '#ff5e62' }
    ]
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad EmiCalPage');

    this.cat_loop = 0;
    this.name = ""; this.mobileNo = ""; this.vehicleNo = ""; this.srcCompany = ""; this.selectedBlocks = []; this.flats = []; this.companyName = ""; this.visitorPic = ''; this.idPic = '';
    this.selectedCompany = { id: 0, name: '' }; this.selectedBlock = {}; this.selectedFlat = {}; this.isShowIdPic = false; this.isShowPic = false;

    this.storage.get('user').then((val) => {
      this.user = (val);
      // this.api.getBlockDetails(this.user['apartmentId'], this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   this.blocks = data;
      // });

      // this.api.getCompanyDetails(this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   this.companies = data;
      //   this.catCompany = this.getCompanies();
      // })
    });
    if (this.rotp) {
      console.log(this.rotp);
    }
  }
  getPicidcardimage(){
    this.camera.getPicture({
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 600,
      targetHeight: 300,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((data) => {
      this.idcardimagepic = "data:image/jpeg;base64," + data;
      this.idcardimage = true;
    }, (error) => {

    });
  }
  getCompanies() {
    let c = '';
    let cc: any[] = [];

    for (let i = 0; i < this.companies.length; i++) {
      if (c !== this.companies[i].companyCategory) {
        let obj = { id: this.companies[i].id, cat: this.companies[i].companyCategory, d: [this.companies[i]] };
        cc.push(obj);
        c = this.companies[i].companyCategory;
      } else {
        cc[cc.length - 1].d.push(this.companies[i]);
      }
    }
    return cc;
  }

  e1: any = '';
  next1(e1) {
    e1.setFocus();
  }

  getPic() {
    this.camera.getPicture({
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 600,
      targetHeight: 300,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((data) => {
      this.visitorPic = "data:image/jpeg;base64," + data;
      this.isShowPic = true;
    }, (error) => {

    });
  }
  getIdPic() {
    this.camera.getPicture({
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 320,
      targetHeight: 320,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((data) => {
      this.idPic = "data:image/jpeg;base64," + data;
      this.isShowIdPic = true;
    }, (error) => {

    });
  }

  addVisitor() {
    if (!this.isShowPic) {
      const toast = this.toastCtrl.create({
        message: 'Add the photo',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return false;
    }
    if (this.isShowIdPic == undefined) {
      const toast = this.toastCtrl.create({
        message: 'ID is required..',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    // Loading
    let loading = this.loadingCtrl.create({
      content: 'Processing ...'
    });
    loading.present();
    var body = {
      mainphoto: this.visitorPic,
      idcard: this.idPic,
      currentPhoto: null
    };
    if (this.name == "") {
      return this.isname = true;
    }
    if (this.existVis != null) {
      if (this.existVis.typeofVistor != this.category) {
        this.visId = '';
      }
    }
    // this.api.doAddVisitor(this.name, this.mobileNo, this.vehicleNo, this.companyName, this.selectedBlockNo, this.selectedBlockLabel, this.user['apartmentId'], this.visId, this.category, this.user['id'], body).then((res) => {
    //   loading.dismiss();
    //   const toast = this.toastCtrl.create({
    //     message: 'Visitor request placed for the approvel..!',
    //     duration: 3000,
    //     position: 'bottom',
    //     cssClass: 'toast-green'
    //   });
    //   toast.present();
    // }, (error) => {
    //   this.error = error;
    // });
    this.navCtrl.push(GatehomePage);
    this.next = 1;
    this.ionViewDidLoad();
    this.sendingreques();
  }
  sendingreques() {
    this.sendingrequest = true;
  }
  searchComapny() {
    // this.api.searchCompany(this.srcCompany, this.user['id']).then((res) => {
    //   const data = JSON.parse(res['_body']);
    //   this.companies = data;
    //   this.companyName = this.srcCompany;
    //   this.catCompany = this.getCompanies();
    //   console.log(this.catCompany);
    // })
  }
  addCompany() {
    // this.api.addCompany(this.companyName, 'Others', '', this.user['id']).then((res) => {
    // });
  }

  onClickBack() {
    if (this.next > 1) {
      this.next--;
    }
  }

  onClickNext() {


    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: 'loading ...',
      duration: 1000
    });
    loading.onDidDismiss(() => {

    })
    loading.present();
    // if (this.next == 1) {
    //   this.onClickOtpVerify();
    //   if (this.wrong) {
    //     return;
    //   }
    // }

    if (this.next == 1) {
      if (this.name == "") {
        return this.isname = true;
      } else {
        this.next++;
      }
    }

    if (this.next == 2) {
      this.notshow = true;
    }
    if (this.next == 3) {
      this.sendingrequest = true;
        
    }

    if (this.next == 4) {

    }
  }

  onClickEvent(item) {
    if (item.typeofVistor == 'maid') {
      this.navCtrl.push(GateformPage, { maid: item });
      this.next--;
    }
    else if (item.typeofVistor == 'driver') {
      this.navCtrl.push(AdddriverPage, { driver: item });
      this.next--;
    }
    /* else if (item.typeofVistor == 'guest') {
      this.vehicleNo = item['vehicleNo'];
      this.name = item['name'];
      this.category = item['typeofVistor'];
      console.log(item);
      this.next+1;
    } */
    this.visId = item['id'];
    this.vehicleNo = item['vehicleNo'];
    this.name = item['name'];
    this.idPic = item['idCard'];
    this.category = item['typeofVistor']
    console.log(item);
    this.next++;
  }

  onClickType(item) {

    if (this.name == '') {
      return this.isname = true;
    }

    this.category = item.cat;
    console.log(this.category);
    this.next++;
  }

  onClickAddNewCompany() {
    if (this.companies.length == 0) {
      const confirm = this.alertCtrl.create({
        title: 'New Company',
        subTitle: 'Name: ' + this.companyName,
        buttons: [
          {
            text: 'Add',
            handler: () => {
              // this.api.addCompany(this.companyName, 'Others', '', this.user['id']).then((res) => {
              //   this.next++;
              // });
            }
          }
        ]
      });
      confirm.present();
      return;
    }
  }

  // triggerOTP() {
  //   this.api.triggerOTP(this.mobileNo, this.user['id']).then((res) => {
  //     this.rotp = res['_body'];
  //     console.log(this.rotp);
  //   });
  // }

  selectedCompany: any = { id: 0, name: '' };
  onClickCompany(company) {
    if (this.companies.length > 0) {
      this.selectedCompany = company;
      this.companyName = company.companyName;
    }
  }

  onClickBlock(block) {
    let curApId = this.user['apartmentId'];
    this.selectedBlock = block;
    this.blockNo = block.name;
    // this.api.getFlatDetails(this.selectedBlock['id'], curApId, this.user['id']).then((res) => {
    //   const data = JSON.parse(res['_body']);
    //   console.log(data);
    //   this.flats = data;
    // })

  }

  onClickFlat(flat) {
    let isflat = false;
    this.selectedFlat = flat;
    if (this.selectedBlocks.length <= 0) {
      this.flatNo = flat.flatName;
      const obj = { block: this.selectedBlock, flat: this.selectedFlat };
      this.selectedBlocks.push(obj);
      this.displayFlat();
    }
    else {
      for (let i of this.selectedBlocks) {
        if (flat.blockId == i.flat.blockId && flat.id == i.flat.id) {
          isflat = true;
          break;
        }
      }
      if (!isflat) {
        this.flatNo = flat.flatName;
        const obj = { block: this.selectedBlock, flat: this.selectedFlat };
        this.selectedBlocks.push(obj);
        this.displayFlat();
      }
    }
  }

  displayFlat() {
    let labelId = "";
    let labelName = "";
    if (this.selectedBlocks.length > 0) {
      for (let i = 0; i < this.selectedBlocks.length; i++) {
        if (i > 0) {
          labelName = `${labelName},`;
          labelId = `${labelId},`;
        }
        const curBlock = this.selectedBlocks[i].block;
        const curFlat = this.selectedBlocks[i].flat;
        labelId = labelId + curBlock.id + "-" + curFlat.id;
        labelName = labelName + curBlock.name + "-" + curFlat.flatName;
      }
    }
    this.selectedBlockNo = labelId;
    this.selectedBlockLabel = labelName;
  }

  onClickClose(idx) {
    this.selectedBlocks = this.selectedBlocks.filter((item, i) => {
      return idx !== i;
    });
    this.displayFlat();
  }

  onClickOtpVerify() {
    if (this.rotp === this.otp) {
      this.verify = true;
      this.wrong = false;
      this.otp = '';
      this.next = 2;
    } else {
      this.wrong = true;
    }
  }

  isCompany() {
    if (this.companyName == '') {
      this.iscompanies = true;
      const toast = this.toastCtrl.create({
        message: 'Please select any company.!',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    else {
      this.next++;
    }
  }

  isBlock() {
    if (this.blockNo == '' || this.flatNo == '') {
      this.isblock = true;
      const toast = this.toastCtrl.create({
        message: 'Please select  a flat.!',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    else {
      if (this.existVis != undefined) {
        this.next = 6;
      }
      else {
        this.next++;
      }
    }
  }
  gotocompany() {
    const myModal: Modal = this.modal.create(SelectCompanyPage);
    myModal.present();

    myModal.onDidDismiss(data => {
      this.companyName = data;
    });
  }

  gotopurpose() {
    const myModal: Modal = this.modal.create(SelectPurposePage);
    myModal.present();

    myModal.onDidDismiss(data => {
      this.title = data;
    });
  }
  gototheaddpicture() {

    this.show = true;
    this.notshow = false;


  }
 
  openflatselected() {
    const profileModal = this.modalCtrl.create(SelectFlatNumberPage, {});
    profileModal.onDidDismiss(data => {
      console.log(data);
      this.selectedData = data;
      this.selectedData.map(data => {

      });
      this.madalDismissData = JSON.stringify(data);
    });
    profileModal.present();
  }
  dismissModal() {
    this.navCtrl.push(HomePage);
  }
}