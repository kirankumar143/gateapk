import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddViolationPage } from './add-violation';

@NgModule({
  declarations: [
    AddViolationPage,
  ],
  imports: [
    IonicPageModule.forChild(AddViolationPage),
  ],
})
export class AddViolationPageModule {}
