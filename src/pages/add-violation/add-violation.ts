import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { Body } from '@angular/http/src/body';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { Camera, CameraOptions } from '@ionic-native/camera';

/**
 * Generated class for the AddViolationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-violation',
  templateUrl: 'add-violation.html',
})
export class AddViolationPage {
  user: {};
  photos = [];
  memPic = "";
  isShowPic = false;
  violationList: any;
  blockList: any;
  flatList: any;
  blockName: string = '';
  flatName: string = '';
  violationPic: string = '';
  violationName: any = '';
  flat: {};
  violation = '';
  block = '';
  flat1 = '';
  description = '';
  base64Image: string;
  required = false;
  required1 = false;
  required2 = false;
  required3 = false;
  number: any;
  disabled = false;
  thumbnail = [];
  bigImg = null;
  bigSize = '0';
  smallImg = [];
  smallSize = '0';
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public storage: Storage, public camera: Camera, public loadingCtrl: LoadingController, public api: ApiProvider, public viewCtrl: ViewController, public alertCtrl: AlertController) {
    this.storage.get('user').then((val) => {
      this.user = (val);
      // this.api.getViolationsList(this.user['apartmentId'], this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   console.log(data);
      //   this.violationList = data;
      // });
      // this.api.getBlockDetails(this.user['apartmentId'], this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   console.log(data);
      //   this.blockList = data;
      // });
    });
  }
  ngOnInit() {
    this.photos = [];
  }
  deletePhoto(index) {
    let confirm = this.alertCtrl.create({
      title: 'Sure you want to delete this photo? There is NO undo!',
      message: '',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        }, {
          text: 'Yes',
          handler: () => {
            console.log('Agree clicked');
            this.photos.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  /* takePhoto(){
   const options : CameraOptions = {
     quality: 20, // picture quality
     destinationType: this.camera.DestinationType.DATA_URL,
     encodingType: this.camera.EncodingType.JPEG,
     mediaType: this.camera.MediaType.PICTURE
   }
   this.camera.getPicture(options) .then((imageData) => {
       this.violationPic = "data:image/jpeg;base64," + imageData;
       this.photos.push(this.violationPic);
       this.photos.reverse();
     }, (err) => {
       console.log(err);
     });
  } */
  onChangeBlock(block) {
    // this.api.getFlatList(block.id, this.user['apartmentId'], this.user['id']).then((res) => {
    //   const data = JSON.parse(res['_body']);
    //   console.log(data);
    //   this.flatList = data;
    // });
  }
  loadImage() {
    const options: CameraOptions = {
      quality: 20,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options).then(imageData => {
      this.violationPic = 'data:image/jpeg;base64,' + imageData;
      this.bigImg = this.violationPic;
      this.generateFromImage(this.bigImg, 200, 200, 0.5, data => {
        this.smallImg.push(data);
        this.smallSize = this.getImageSize(this.smallImg);
      });
    }, err => {
      console.log('gallery error: ', err);
    });
   
  }
  generateFromImage(img, MAX_WIDTH: number = 700, MAX_HEIGHT: number = 700, quality: number = 1, callback) {
    var canvas: any = document.createElement("canvas");
    var image = new Image();

    image.onload = () => {
      var width = image.width;
      var height = image.height;

      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }
      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext("2d");

      ctx.drawImage(image, 0, 0, width, height);

      // IMPORTANT: 'jpeg' NOT 'jpg'
      var dataUrl = canvas.toDataURL('image/jpeg', quality);

      callback(dataUrl)
    }
    image.src = img;
  }

  getImageSize(data_url) {
    var head = 'data:image/jpeg;base64,';
    return ((data_url.length - head.length) * 3 / 4 / (1024 * 1024)).toFixed(4);
  }
  AddViolation() {
    var body = {
      image1: this.smallImg[0],
      image2: this.smallImg[1],
      image3: this.smallImg[2],
      mainImage1: this.photos[0],
      mainImage2: this.photos[1],
      mainImage3: this.photos[2]
    }
    if (this.violation == '') {
      return this.required = true;
    }
    if (this.block == '') {
      return this.required1 = true;
    }
    if (this.flat1 == '') {
      return this.required2 = true;
    }
    if (this.description == '') {
      return this.required3 = true;
    }
    let addViolationLoader = this.loadingCtrl.create({
      content: 'adding violation'
    });
    addViolationLoader.present();
    // this.api.addViolation(this.user['apartmentId'], this.block['id'], this.flat1['id'], this.block['name'] + '-' + this.flat1['flatName'],
    //   this.violation['id'], this.number, this.user['username'], this.description,
    //   this.violation['name'], this.user['id'], body).then((res) => {
    //     const data = JSON.parse(res['_body']);
    //     console.log(data);
    //     addViolationLoader.dismiss();
    //     const toast = this.toastCtrl.create({
    //       message: 'New Violation is added successfully.!',
    //       duration: 1000,
    //       position: 'bottom',
    //       cssClass: 'toast-green'
    //     });
    //     toast.present();
    //     this.navCtrl.pop();
    //     console.log(data);
    //   })
  }

  onClickCancel() {
    this.viewCtrl.dismiss();
  }
}
