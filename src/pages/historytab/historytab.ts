import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';
/**
 * Generated class for the HistorytabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-historytab',
  templateUrl: 'historytab.html',
})
export class HistorytabPage {
  History = 'h48hrslist';
  user ={};
  apartmentId :string;
  fourtyeightList: any [];
  activeList: any [];

  constructor(public alertCtrl: AlertController,public storage : Storage, public api :ApiProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.storage.get('user').then((val) => {
      this.user = (val);
      this.apartmentId = this.user["apartmentId"];
      // this.api.history48(this.apartmentId,this.user['id']).then((res) =>{
      //   const data = JSON.parse(res['_body']);
      //   console.log(data);
      //   this.fourtyeightList = data;
      // });
        // this.api.historyActive(this.apartmentId,this.user['id']).then((res) =>{
        //   const data = JSON.parse(res['_body']);
        //   console.log(data);
        //   this.activeList = data;
        // });
      });
    }
    getClass(i){
      if(i%2==0){
        return 'bg-green';
      }
      else if(i%3==0){
        return 'bg-red';
      }
      else if(i%4==0){
        return 'bg-blue';
      }
      else if(i%5==0){
        return 'bg-goldenrod';
      }
      else{
        return 'bg-purple';
      }
    }
    getClasss(j){
      if(j%2==0){
        return 'bg-green';
      }
      else if(j%3==0){
        return 'bg-red';
      }
      else if(j%4==0){
        return 'bg-blue';
      }
      else if(j%5==0){
        return 'bg-goldenrod';
      }
      else{
        return 'bg-purple';
      }
    }

    onClickOut(item) {
      const confirm = this.alertCtrl.create({
        title: 'Issueing Out Gate Pass',
        subTitle: 'Name: ' + item.name,
        buttons: [
          {
            text: 'Issue Out Pass',
            handler: () => {
              // this.api.onOut(item.id, this.user['apartmentId'], item.plotNumber, item.rId,this.user['id']).then((res)=>{
              // });
            }
          }
        ]
      });

      confirm.present();
    }
  }
