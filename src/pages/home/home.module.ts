import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';

import {IonMarqueeModule} from "ionic-marquee";

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    IonMarqueeModule,
  ],
})
export class HomePageModule {}
