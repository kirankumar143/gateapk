import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, Modal, PopoverController, AlertController, ToastController, Popover, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';
import { StaffpopoverComponent } from '../../components/staffpopover/staffpopover';
import { StaffloginpopoverComponent } from '../../components/staffloginpopover/staffloginpopover';
import { VisdetailsPage } from '../visdetails/visdetails';
import { InsidePage } from '../inside/inside';
import { WaitingPage } from '../waiting/waiting';
import { NotesPage } from '../notes/notes';
import { VerificationpagePage } from '../verificationpage/verificationpage';
import { Camera } from '@ionic-native/camera';
import { SelectCompanyPage } from '../select-company/select-company';
import { SelectFlatNumberPage } from '../select-flat-number/select-flat-number';
import { SelectPurposePage } from '../select-purpose/select-purpose';
import { ViolationListPage } from '../violation-list/violation-list';
import { ExpVisPage } from '../exp-vis/exp-vis';
import { AddNewStaffPage } from '../add-new-staff/add-new-staff';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  horizontalText = '';
  navctrl: any;
  userDetailsFrom: any;
  modalOpen: boolean;
  plotName: any;
  typeofVistor: any;
  show: boolean;
  selectedData: any;
  title: any;
  title1: any;
  otpverification: any;
  public toggled: boolean = false;
  sendingrequest: boolean;
  apartmentId: string;
  stafflist: any[] = [];
  rId = '';
  closebreak: Date;
  vislist: any = [];
  retiredlist: any[] = [];
  isScroll = true;
  currentPhoto: string;
  search = "";
  vlist: any = [];
  user: any;
  wait: number = 1;
  srcVis = "";
  lsize = 0;
  page = false;
  isData = false;
  pollStart = false;
  existVis: any = [];
  maidList: any;
  maidId: string;
  maidStatus: string;
  driverList: any;
  driverId: string;
  driverStatus: string;
  busList: any;
  isArrive = false;
  isLeft = true;
  busId: string;
  selBus: any[] = [];
  item = [];
  guestList: any;
  expVisList: any;
  deliverylist: any;
  othersList: any;
  segments: any;
  notshow: boolean;
  isShowPic = false;
  isShowPic2 = false;
  isShowIdPic = false;
  segmentss: any;
  mob = false;
  mobileNo = "";
  next: number = 1;
  cat_loop: number;
  resCat = [];
  categories: any;
  visId = '';
  vehicleNo: string;
  name = "";
  idPic = "";
  wrong = false;
  isname = false;
  companies = [];
  companyName: string = "";
  rotp: string;
  otp: string;
  verify = false;
  visitorPic = "";
  visitorPic2 = "";
  madalDismissData: any;
  username: string;


  constructor(public loadcontroller: LoadingController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public popoverCtrl: PopoverController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public api: ApiProvider,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public modal: ModalController,

    public camera: Camera, ) {

    this.toggled = false;
    this.existVis = this.navParams.get('mobile');
    this.segments = "add";
    this.segmentss = "segments1";
    this.doRefresh;

  }

  ngOnInit() {
    setTimeout(() => {
      if (this.user.username) {
        this.horizontalText = 'Security Guard : ' + this.user.username + ' , Logged in at 08:45 pm hrs';
      }
    }, 10000);
  }

  // ionViewWillEnter() {

  //   this.storage.get('user').then((res) => {
  //     this.user = res;
  //     console.log(this.user);
  //   });
  //   this.storage.get('user').then(val => {
  //     this.user = (val);
  //     console.log(this.user);
  //     this.api.getExpVisList(this.user['apartmentId'], this.user['id'], this.search).then(res => {
  //       const data = JSON.parse(res['_body']);
  //       console.log(data);
  //       if (data != null) {
  //         this.expVisList = data;
  //       }
  //       else {
  //         this.expVisList = 0;
  //       }
  //     });
  //    this.apartmentId = this.user["apartmentId"];
  //     this.api.getVisitorsList(this.user["apartmentId"], '10000', this.srcVis, 'guest', this.user['id']).subscribe((res) => {
  //       const data = JSON.parse(res['_body']);
  //       console.log(data);
  //       this.page = true;
  //       if (data.list !== null) {
  //         this.guestList = data.list;
  //         this.page = false;
  //       }
  //       else {
  //         this.isData = true;
  //         this.page = false;
  //       }
  //     });
  //     this.api.getSchoolbusList(this.user["apartmentId"], this.user['id'], this.search).subscribe((res) => {
  //       const data = JSON.parse(res['_body']);
  //       this.page = true;
  //       console.log(data);
  //       if (data !== null) {
  //         this.busList = data;
  //         this.page = false;
  //       }
  //       else {
  //         this.isData = true;
  //         this.page = false;
  //       }
  //     });
  //     this.apartmentId = this.user["apartmentId"];
  //     this.api.staffList(this.apartmentId, '10000', this.search, this.user['id']).then((res) => {
  //       const data = JSON.parse(res['_body']);
  //       console.log(data);
  //       this.stafflist = data;
  //     });
  //     this.api.staffRetireList(this.apartmentId, this.user['id']).then((res) => {
  //       const data = JSON.parse(res['_body']);
  //       this.retiredlist = data;

  //     });
  //     if (this.existVis) {
  //       this.vlist.push(this.existVis);
  //       console.log(this.vlist);
  //     } else {
  //       this.api.getAllVistorsList(this.user["apartmentId"], '10000', this.srcVis, this.user['id']).subscribe((res) => {
  //         const data = JSON.parse(res['_body']);
  //         console.log(data);
  //         this.page = true;
  //         if (data.list !== null) {
  //           this.vlistUpdat(data.list);
  //           console.log(data);
  //           this.page = false;
  //         }
  //         else {
  //           this.vlist = 0;
  //           this.isData = true;
  //           this.page = false;
  //         }
  //       });
  //     }
  //     this.apartmentId = this.user["apartmentId"];
  //     this.api.getVisitorsList(this.user["apartmentId"], '10000', this.srcVis, 'delivery', this.user['id']).subscribe((res) => {
  //       const data = JSON.parse(res['_body']);

  //       this.page = true;
  //       if (data.list !== null) {
  //         this.vlistUpdate(data.list);
  //         this.page = false;
  //         console.log(data);
  //       }
  //       else {
  //         this.isData = true;
  //         this.page = false;
  //       }
  //     });
  //     this.apartmentId = this.user["apartmentId"];
  //     this.api.getVisitorsList(this.apartmentId, '10000', this.srcVis, 'maid', this.user['id']).subscribe((res) => {
  //       const data = JSON.parse(res['_body']);
  //       console.log(data);
  //       this.page = true;
  //       if (data.list !== null) {
  //         this.maidList = data.list;
  //         this.page = false;
  //       }
  //       else {
  //         this.isData = true;
  //         this.page = false;
  //       }
  //     });
  //     this.apartmentId = this.user["apartmentId"];
  //     this.api.getVisitorsList(this.apartmentId, '10000', this.srcVis, 'driver', this.user['id']).subscribe((res) => {
  //       const data = JSON.parse(res['_body']);
  //       console.log(data);
  //       this.page = true;
  //       if (data.list !== null) {
  //         this.driverList = data.list;
  //         this.page = false;
  //       }
  //       else {
  //         this.isData = true;
  //         this.page = false;
  //       }
  //     });
  //     this.apartmentId = this.user["apartmentId"];
  //     this.api.getVisitorsList(this.apartmentId, '10000', this.srcVis, 'others', this.user['id']).subscribe((res) => {
  //       const data = JSON.parse(res['_body']);
  //       console.log(data);
  //       this.page = true;
  //       console.log(data);
  //       if (data.list !== null) {
  //         this.vlistUpdate1(data.list);
  //         this.page = false;
  //         console.log(data);

  //       }
  //       else {
  //         this.isData = true;
  //         this.page = false;
  //       }
  //     });

  //   });

  // };




  // onClickLogout(item) {
  //   if (item.breakStatus != 'yes') {
  //     const confirm = this.alertCtrl.create({
  //       title: 'Are you sure..!',
  //       subTitle: 'to ' + '<font color="red" size="4">LogOut</font> the entry of ' + '<br><b>' + item.name + '</b>',
  //       buttons: [
  //         {
  //           text: 'Cancel',
  //           handler: () => {
  //           }
  //         },
  //         {
  //           text: 'Yes',
  //           handler: () => {
  //             this.api.dayOut(item.rId, item.id, this.user['id']).then(() => {
  //               item.access = 'left';
  //               item.outTime = new Date();
  //             });
  //           }
  //         }

  //       ]
  //     });
  //     confirm.present();
  //   } else {
  //     const toast = this.toastCtrl.create({
  //       message: 'Please close break after do Logout',
  //       duration: 3000,
  //       position: 'bottom',
  //       cssClass: 'toast-orange'
  //     });
  //     toast.present();
  //   }

  // }
  vlistUpdate1(vlist) {
    this.othersList = vlist.map(dat => {
      dat.plotName = dat.plotName.split(',');
      dat.blockNo = dat.blockNo.split(',').map((flt, index) => {
        flt = flt.split('/');
        return { name: dat.plotName[index], status: flt[1] };
      });
      return dat;
    });
  }
  // doScrolling() {

  //   const tmp = this.stafflist[this.stafflist.length - 1];
  //   setTimeout(() => {
  //     this.isScroll = false;
  //     this.api.staffList(this.apartmentId, tmp['id'], this.search, this.user['id']).then((res) => {
  //       const data = JSON.parse(res['_body']);
  //       console.log(data);
  //       if (data !== null) {
  //         this.stafflist = [...this.stafflist, ...data];
  //       }
  //       this.isScroll = true;
  //     });
  //   }, 1000);
  // }
  gototheaddpicture() {

    this.show = true;
    this.notshow = false;


  }

  public toggle(): void {
    this.toggled = !this.toggled;

  }
  vlistUpdate(vlist) {
    this.deliverylist = vlist.map(dat => {
      dat.plotName = dat.plotName.split(',');
      dat.blockNo = dat.blockNo.split(',').map((flt) => {
        flt = flt.split('/');
        const numbers = flt[0].split('-');
        return { name: `${numbers[0]}-${numbers[1]}`, status: flt[1] };
      });
      dat.blockNo = Array.from(new Set(dat.blockNo.map(i => i.name))).map((k, i) => {
        return { name: dat.plotName[i], status: dat.blockNo.find(f => f.name === k).status };
      });
      return dat;
    });
  }
  vlistUpdat(vlist) {
    if (this.existVis) {
      this.existVis = vlist.map(dat => {
        console.log(dat);
        dat.plotName = dat.plotName.split(',');
        dat.blockNo = dat.blockNo.split(',').map((flt) => {
          flt = flt.split('/');
          const numbers = flt[0].split('-');
          return { name: `${numbers[0]}-${numbers[1]}`, status: flt[1] };

          /* return { name: dat.plotName[index], status: flt[1] }; */
        });
        dat.blockNo = Array.from(new Set(dat.blockNo.map(i => i.name))).map((k, i) => {
          return { name: dat.plotName[i], status: dat.blockNo.find(f => f.name === k).status };
        });
        return dat;
      });
    }
    else {
      this.vlist = vlist.map(dat => {
        console.log(dat);
        dat.plotName = dat.plotName.split(',');
        dat.blockNo = dat.blockNo.split(',').map((flt) => {
          flt = flt.split('/');

          const numbers = flt[0].split('-');
          return { name: `${numbers[0]}-${numbers[1]}`, status: flt[1] };
        });
        dat.blockNo = Array.from(new Set(dat.blockNo.map(i => i.name))).map((k, i) => {
          return { name: dat.plotName[i], status: dat.blockNo.find(f => f.name === k).status };
        });

        return dat;
      });
    }
  }



  backbutton() {
    this.toggled = false;
  }


  getClass(i) {
    if (i % 2 == 0) {
      return 'bg-green';
    }
    else if (i % 3 == 0) {
      return 'bg-red';
    }
    else {
      return 'bg-blue';
    }
  }

  presentPopover(event, item) {
    const popover: Popover = this.popoverCtrl.create(StaffpopoverComponent, { item: item });
    popover.present({
      ev: event
    });
    popover.onDidDismiss(() => {
      // this.ionViewWillEnter();
    })
  }

  prePopover(item) {
    const popover = this.popoverCtrl.create(StaffloginpopoverComponent, { item: item });
    popover.present();
    console.log(item);
  }

  editid() {

  }
  doRefresh(refresher) {
    // this.ionViewWillEnter();
    refresher.complete();
  }


  onClickAllow(item) {
    event.stopPropagation();
    const confirm = this.alertCtrl.create({
      title: 'Issueing In Gate Pass',
      subTitle: 'Name: ' + item.name,
      buttons: [
        {
          text: 'Issue In Pass',
          handler: () => {
            let flats: string = '';
            for (let i of item.plotName) {
              flats += i + ',';
            }
            // this.api.allowVisitor(item.vistorId, item.rId, this.user['username'], 'vistor', flats, this.user['id']).then(res => {
            //   this.ionViewWillEnter();
            // });
          }
        }
      ]
    });
    confirm.present();
  }


  onClickOut(item) {
    /* let date = new Date(parseInt(item.inTime)).toISOString().substring(0,10);
    let time = new Date(parseInt(item.inTime)).toISOString().substring(11,19);
    let data = date+' '+time; */
    event.stopPropagation();
    const confirm = this.alertCtrl.create({
      title: 'Issueing Out Gate Pass',
      subTitle: 'Name: ' + item.name,
      buttons: [
        {
          text: 'Issue Out Pass',
          handler: () => {
            let flats: string = '';
            for (let i of item.plotName) {
              flats += i + ',';
            }
            // this.api.onOut(item.vistorId, this.user['apartmentId'], flats, item.rId, this.user['id']).then((res) => {

            //   this.ionViewWillEnter();
            // });
          }
        }
      ]
    });

    confirm.present();
  }
  onClickArrow(event, item) {
    event.stopPropagation();
    item.id = item.vistorId;
    this.navCtrl.push(VisdetailsPage, { item: item });
  }

  gotocompany() {
    const myModal: Modal = this.modal.create(SelectCompanyPage);
    myModal.present();

    myModal.onDidDismiss(data => {
      this.companyName = data;
    });
  }
  onClickNext() {

    if (this.next == 1) {
      if (this.mobileNo == "" || Number(this.mobileNo.length) < 10) {
        return this.mob = true;
      }
      else {
        // document.getElementById("Div1").style.display = "none";
        // document.getElementById("Div2").style.display = "none";
        // this.api.getExistAllDetails(this.mobileNo, this.user['id']).then((res) => {
        //   const data = JSON.parse(res['_body']);
        //   this.existVis = data;
        //   if (this.existVis != undefined) {
        //     this.existVis = data.sort(function (a, b) {
        //       return parseFloat(b.id) - parseFloat(a.id);
        //     });
        //     this.existVis = data.reduce(
        //       (accumulator, current) => accumulator.some(x => x.typeofVistor === current.typeofVistor) ? accumulator : [...accumulator, current], []
        //     )

        //     if (this.existVis[0].access != 'left' && this.existVis[0].access != 'declined' && this.existVis[0].access != 'created' && this.existVis[0].access != 'pending') {
        //       this.navCtrl.push(HomePage, { visitor: this.existVis });
        //     } else {
        //       if (this.cat_loop == 0) {
        //         for (let i = 0; i < this.categories.length; i++) {
        //           let j;
        //           loop1:
        //           for (j = 0; j < this.existVis.length; j++) {
        //             if (this.existVis[j].typeofVistor == this.categories[i].cat) break loop1;
        //           }
        //           if (j == this.existVis.length)
        //             this.resCat.push(this.categories[i]);
        //         }
        //         this.cat_loop++;
        //       }
        //       console.log(this.resCat);
        //       this.visId = this.existVis[0].id;
        //       this.vehicleNo = this.existVis[0].vehicleNo;
        //       this.name = this.existVis[0].name;
        //       this.companyName = this.existVis[0].companyName;
        //       this.plotName = this.existVis[0].plotName;
        //       this.typeofVistor = this.existVis[0].typeofVistor;
        //       this.idPic = this.existVis[0].idCard;
        //       console.log(this.existVis);
        //       this.next = 3;
        //       // document.getElementById("Div1").style.display = "none";
        //       // document.getElementById("Div2").style.display = "none";
        //     }

        //   }                               
        //   else {
        //     // document.getElementById("Div1").style.display = "none";
        //     // document.getElementById("Div2").style.display = "none";
        //       // this.toolbarhide = false;
        //       // this.toolbarhide2 = true;
        //     // this.otpverification;
        //     this.next = 2;
        //     this.triggerOTP();
        //     if (this.cat_loop == 0) {
        //       for (let i = 0; i < this.categories.length; i++) {
        //         this.resCat.push(this.categories[i]);
        //       }
        //       this.cat_loop++;

        //     }
        //   }
        // });
      }
    }
    if (this.next == 2) {
      // document.getElementById("Div1").style.display = "none";
      // document.getElementById("Div2").style.display = "none";
      this.onClickOtpVerify();
      if (this.wrong) {
        return;
      }
      this.navCtrl.push(VerificationpagePage, { mobileNo: this.mobileNo });
    }
    if (this.next == 3) {
      this.next = 4;

    }
    if (this.next == 4) {
      // this.next=5;
      //  this.sendingrequest = true;

    }

  }
  sending() {
    this.next = 5;
    // this.sendingrequest = true;
  }
  gotohome() {
    this.navCtrl.push(HomePage);
  }
  getPic() {
    this.camera.getPicture({
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 600,
      targetHeight: 300,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((data) => {
      this.visitorPic = "data:image/jpeg;base64," + data;
      this.isShowPic = true;
    }, (error) => {

    });
  }
  getPic2() {
    this.camera.getPicture({
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 600,
      targetHeight: 300,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((data) => {
      this.visitorPic2 = "data:image/jpeg;base64," + data;
      this.isShowPic2 = true;
    }, (error) => {

    });
  }
  triggerOTP() {
    // this.api.triggerOTP(this.mobileNo, this.user['id']).then((res) => {
    //   this.rotp = res['_body'];
    //   console.log(this.rotp);
    //   this.storage.set('rotp', 'this.rotp');
    // });
    // this.navCtrl.push(VerificationpagePage ,{item:this.rotp});
  }

  onPageWillEnter() {
  }

  submitUserDetail() {

    let userData = this.userDetailsFrom.value;
    let modal = this.modalCtrl.create(VerificationpagePage, { data: userData });

    modal.present().then((res) => {
      res ? this.modalOpen = true : this.modalOpen = false;
    }).catch((err) => {
      console.log("error is", err)
    })
    modal.onDidDismiss(() => { this.modalOpen = false; })
  }
  onClickOtpVerify() {


    if (this.rotp === this.otp) {
      this.verify = true;
      this.wrong = false;
      this.otp = '';
      this.next = 2;
    } else {
      this.wrong = true;
    }
  }
  openflatselected() {
    const profileModal = this.modalCtrl.create(SelectFlatNumberPage, {});
    profileModal.onDidDismiss(data => {
      console.log(data);
      this.selectedData = data;
      this.selectedData.map(data => {

      });
      this.madalDismissData = JSON.stringify(data);
    });
    profileModal.present();
  }
  dismissModal() {
    this.navCtrl.push(HomePage);
  }
  gotopurpose() {
    const myModal: Modal = this.modal.create(SelectPurposePage);
    myModal.present();

    myModal.onDidDismiss(data => {
      this.title1 = data;
    });
  }
  gotoback(){
    this.navCtrl.push(HomePage);
  }
  getIdPic() {
    this.camera.getPicture({
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 320,
      targetHeight: 320,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((data) => {
      this.idPic = "data:image/jpeg;base64," + data;
      this.isShowIdPic = true;
    }, (error) => {

    });
  }
  
  inside() {
    this.navCtrl.push(InsidePage);
  }
  waiting() {
    this.navCtrl.push(WaitingPage);
  }
  expected() {
    this.navCtrl.push(ExpVisPage);
  }
  violation() {
    this.navCtrl.push(ViolationListPage);

  }
 
  notes() {
    this.navCtrl.push(NotesPage);
  }
  gotothenewstff(){
    this.navCtrl.push(AddNewStaffPage);
  }
}














