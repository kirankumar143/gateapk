import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the H48hrslistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-h48hrslist',
  templateUrl: 'h48hrslist.html',
})
export class H48hrslistPage {
  user : string;
  apartmentId : string;
  detailsList : any;
  page = false;

  constructor(public loadingCtrl: LoadingController,public storage: Storage, public navCtrl: NavController, public navParams: NavParams,public api: ApiProvider) {
  }

  ionViewDidLoad() {
    // Loading
    let loading = this.loadingCtrl.create({
      content:'Processing ...'
    });
    loading.present();
    this.storage.get('user').then((val) => {
      this.user = (val);
      this.apartmentId = this.user["apartmentId"];
      // this.api.history48(this.apartmentId,this.user['id']).then((res) =>{
      //   loading.dismiss();
      //   const data = JSON.parse(res['_body']);
      //   if(data !== null){
      //     this.detailsList = data;
      //     this.page = false;
      //   }
      // });
    });
    if(this.detailsList == undefined) {
      this.page = !this.page;
    }
  }

  doRefresher(refresher){
    this.ionViewDidLoad();
    refresher.complete();
  }

}
