import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AddguestPage } from '../addguest/addguest';
import { GuestlistPage } from '../guestlist/guestlist';

/**
 * Generated class for the GuesttabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-guesttab',
  templateUrl: 'guesttab.html',
})
export class GuesttabPage {

  user: {};

  constructor(public storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.storage.get('user').then((res)=>{
      this.user = (res);
    });
  }
  tab1Root = AddguestPage;
  tab2Root = GuestlistPage;
}
