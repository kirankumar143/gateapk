import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllVisitorsListPage } from './all-visitors-list';

@NgModule({
  declarations: [
    AllVisitorsListPage,
  ],
  imports: [
    IonicPageModule.forChild(AllVisitorsListPage),
  ],
})
export class AllVisitorsListPageModule {}
