import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';
/**
 * Generated class for the AllVisitorsListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-all-visitors-list',
  templateUrl: 'all-visitors-list.html',
})
export class AllVisitorsListPage {
  user: {};
  vlist: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public alertCtrl: AlertController, public api: ApiProvider) {

  }

  ionViewWillEnter() {
    this.vlist = this.navParams.get('visitor');
    console.log(this.vlist);
    this.storage.get('user').then((val) => {
      this.user = (val);

    })
  }
  onClickOut(item) {
    event.stopPropagation();
    const confirm = this.alertCtrl.create({
      title: "Issuing Out gate pass",
      subTitle: 'Name: ' + item.name,
      buttons: [
        {
          text: 'Issue out pass',
          handler: () => {
            // this.api.onOut(item.id, this.user['apartmentId'], item.plotName, item.rId, this.user['id']).then((res) => {
            //   this.ionViewWillEnter();
            // })
          }
        }
      ]
    });
    confirm.present();
    this.navCtrl.pop();
  }
  getClass(i) {
    if (i % 2 == 0) {
      return 'bg-green';
    }
    else if (i % 3 == 0) {
      return 'bg-red';
    }
    else if (i % 4 == 0) {
      return 'bg-blue';
    }
    else if (i % 5 == 0) {
      return 'bg-goldenrod';
    }
    else {
      return 'bg-purple';
    }
  }
}
