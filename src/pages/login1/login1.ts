import { Component } from '@angular/core';
import { GatehomePage } from '../gatehome/gatehome';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';

/**
 * Generated class for the Login1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login1',
  templateUrl: 'login1.html',
})
export class Login1Page {

  isLoginFail = false;
  password: string;

  constructor(public storage: Storage, public navCtrl: NavController, public navParams: NavParams, public api: ApiProvider) {
  }

  ionViewDidLoad() {
    this.storage.get('user').then((val) => {
      const user = val;
      if(user && user.id) {
        this.isLoginFail = false;
        this.navCtrl.setRoot(HomePage);
      }
    });
  }
  onLoginClick() {
    // this.api.doLogin(this.password).then((res) => {
    //   let data = null;

    //   try{
    //   data = JSON.parse(res['_body']);
    //   console.log(data);
    //   if(data.id) {
    //     this.storage.set('user', data);
    //     this.isLoginFail = false;
    //     this.navCtrl.setRoot(HomePage);
    //   } else {
    //     this.isLoginFail = true;
    //   }
    //   }catch(e) {
    //    this.isLoginFail = true;
    //   }
    // },(error) => {
    //   this.isLoginFail = true;
    // });

  }
}
