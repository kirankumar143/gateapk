import { Component } from '@angular/core';
import  { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { AddlistvisitortabPage } from '../addlistvisitortab/addlistvisitortab';
import { AddlistmaidtabPage } from '../addlistmaidtab/addlistmaidtab';
import { DrivertabPage } from '../drivertab/drivertab';
import { DeliveryboytabPage } from '../deliveryboytab/deliveryboytab';
import { BusPage } from '../bus/bus';
import { OtherstabPage } from '../otherstab/otherstab';
import { StaffsPage } from '../staffs/staffs';
import { GuesttabPage } from '../guesttab/guesttab';
import { ExpVisPage } from '../exp-vis/exp-vis';
import { KeepatgatetabPage } from '../keepatgatetab/keepatgatetab';
import { ParkingSlotsPage } from '../parking-slots/parking-slots';
import { ViolationListPage } from '../violation-list/violation-list';
import { AddAllPage } from '../add-all/add-all';
import { VisitorlistPage } from '../visitorlist/visitorlist';
import { MaidlistPage } from '../maidlist/maidlist';
import { DriverlistPage } from '../driverlist/driverlist';
import { DeliveryboylistPage } from '../deliveryboylist/deliveryboylist';
import { OtherslistPage } from '../otherslist/otherslist';
import { GuestlistPage } from '../guestlist/guestlist';
import { HistorytabPage } from '../historytab/historytab';
/**
 * Generated class for the GatehomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gatehome',
  templateUrl: 'gatehome.html',
})
export class GatehomePage {

  constructor(public storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
  }

  user: {};
  ionViewDidLoad() {
    this.storage.get('user').then((val) =>  {
    this.user = (val);
    });
  }
  onClickAll(){
    this.navCtrl.push(AddAllPage);
  }
  onClickVisitors(){
    this.navCtrl.push(VisitorlistPage);
  }

  onClickMaid(){
    this.navCtrl.push(MaidlistPage);
  }

  onClickDriver() {
    this.navCtrl.push(DriverlistPage);
  }
  onClickDeliveryboy(){
    this.navCtrl.push(DeliveryboylistPage);
  }
  onClickBus(){
    this.navCtrl.push(BusPage);
  }
  onClickOthers(){
    this.navCtrl.push(OtherslistPage);
  }
  onClickStaff(){
    this.navCtrl.push(StaffsPage);
  }
  onClickGuest(){
    this.navCtrl.push(GuestlistPage);
  }
  onClickExpVis(){
    this.navCtrl.push(ExpVisPage);
  }
  onClickKeepatgate(){
    this.navCtrl.push(KeepatgatetabPage);
  }
  onClickParkingSlotList(){
    this.navCtrl.push(ParkingSlotsPage);
  }
  searchViolations(){
    this.navCtrl.push(ViolationListPage);
  }
  clickHistory(){
    this.navCtrl.push(HistorytabPage);
  }
}