import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IssueddellistPage } from './issueddellist';

@NgModule({
  declarations: [
    IssueddellistPage,
  ],
  imports: [
    IonicPageModule.forChild(IssueddellistPage),
  ],
})
export class IssueddellistPageModule {}
