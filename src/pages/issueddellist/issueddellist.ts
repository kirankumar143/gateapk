import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the IssueddellistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-issueddellist',
  templateUrl: 'issueddellist.html',
})
export class IssueddellistPage {

  user: {};
  recievedList: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public storage: Storage,
    ) { }

  ionViewWillEnter(){
    this.storage.get('user').then(val =>{
      this.user = (val);
      // this.api.getRecievedDeliveryList(this.user['apartmentId'],this.user['id']).then(res =>{
      //   const data = JSON.parse(res['_body']);
      //   if(data!=null){
      //     this.recievedList = data;
      //   }
      //   else{
      //     this.recievedList = 0;
      //   }
      //   console.log(this.recievedList);
      // })
    })
  }

  doRefresh(refresher){
    this.ionViewWillEnter();
    refresher.complete();
  }

}
