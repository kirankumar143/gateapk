import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { GatevisitorsPage } from '../gatevisitors/gatevisitors';
import { VisitorlistPage } from '../visitorlist/visitorlist';

/**
 * Generated class for the AddlistvisitortabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addlistvisitortab',
  templateUrl: 'addlistvisitortab.html',
})
export class AddlistvisitortabPage {

  constructor(public storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
  }

  user: {};

  ionViewDidLoad() {
    this.storage.get('user').then((val) => {
    this.user = (val);
    });

  }

  tab1Root = GatevisitorsPage;
  tab2Root = VisitorlistPage;

}
