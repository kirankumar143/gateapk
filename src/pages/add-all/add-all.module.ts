import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddAllPage } from './add-all';

@NgModule({
  declarations: [
    AddAllPage,
  ],
  imports: [
    IonicPageModule.forChild(AddAllPage),
  ],
})
export class AddAllPageModule {}
