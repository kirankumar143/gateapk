import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { AllVisitorsListPage } from '../all-visitors-list/all-visitors-list';
import { GatehomePage } from '../gatehome/gatehome';
import { GateformPage } from '../gateform/gateform';
import { AdddriverPage } from '../adddriver/adddriver';
/**
 * Generated class for the AddAllPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-all',
  templateUrl: 'add-all.html',
})
export class AddAllPage {
  mobileNo = "";
  mob = false;
  name = "";
  isname = false;
  companyName = "";
  vehicleNo: string;
  blockNo = "";
  flatNo = "";
  photoURL: string;
  next: number = 1;
  isDetails = true;
  isShowCompanies = false;
  selectedBlocks = [];
  isblock = false;
  user: {};
  selected: {};
  company: {};
  visitorPic = "";
  isvisitor = false;
  isShowPic = false;
  isEnabled = true;
  blocks = [];
  companies = [];
  iscompanies = false;
  flats = [];
  idPic = "";
  verify = false;
  isShowIdPic = false;
  catCompany: any[] = [];
  srcCompany: string;
  error: any;
  mobilePattern: string;
  otp: string;
  rotp: string;
  selectedBlockLabel: string;
  selectedBlockNo: string;
  selectedFlat = {};
  selectedBlock = {};
  wrong = false;
  visId='';
  existVis: any;
  required = true;
  profession = "";
  isprofession = false;
  category: any = [];
  resCat = [];
  categories: any;
  cat_loop: number;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public api: ApiProvider,
    public storage: Storage,
    public camera: Camera,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
  ) {
    this.categories = [
      { cat: 'vistor', icon: '../../assets/imgs/visitor.png', style: '#5a056b' },
      { cat: 'delivery', icon: '../../assets/imgs/delivery.svg', style: '#ee4b19' },
      { cat: 'others', icon: '../../assets/imgs/others.png', style: '#f2994a' },
      { cat: 'guest', icon: '../../assets/imgs/Guests.png', style: '#ff5e62' }
    ]
    /* this.categories = [
      { cat: 'vistor', icon: '../../assets/imgs/visitor.png',style:{bgcolor: '#12c150',bgimg: 'linear-gradient(to bottom, #a8e063, #12c150)'}},
      { cat: 'delivery', icon: 'ios-cart',style:{bgcolor: '#80070d',bgimg: 'linear-gradient(to bottom, #db8c15, #80070d)'}},
      { cat: 'others', icon: 'people', style:{bgcolor: '#8f94fb',bgimg: 'linear-gradient(to bottom, #9841ca, #0c15c4)'}},
      { cat: 'guest', icon: 'ios-man', style:{bgcolor: '#ffc617',bgimg: 'linear-gradient(to bottom, #ffc617, #f2994a)'}}
    ] */
  }

  ionViewDidLoad() {
    this.cat_loop = 0;
    this.name = ""; this.mobileNo = ""; this.vehicleNo = ""; this.srcCompany = ""; this.selectedBlocks = []; this.flats = []; this.companyName = ""; this.visitorPic = ''; this.idPic = '';
    this.selectedCompany = { id: 0, name: '' }; this.selectedBlock = {}; this.selectedFlat = {}; this.isShowIdPic = false; this.isShowPic = false;

    this.storage.get('user').then((val) => {
      this.user = (val);
      // this.api.getBlockDetails(this.user['apartmentId'], this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   this.blocks = data;
      // });

      // this.api.getCompanyDetails(this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   this.companies = data;
      //   this.catCompany = this.getCompanies();
      // })
    });
  }

  getCompanies() {
    let c = '';
    let cc: any[] = [];

    for (let i = 0; i < this.companies.length; i++) {
      if (c !== this.companies[i].companyCategory) {
        let obj = { id: this.companies[i].id, cat: this.companies[i].companyCategory, d: [this.companies[i]] };
        cc.push(obj);
        c = this.companies[i].companyCategory;
      } else {
        cc[cc.length - 1].d.push(this.companies[i]);
      }
    }
    return cc;
  }

  e1: any = '';
  next1(e1) {
    e1.setFocus();
  }

  getPic() {
    this.camera.getPicture({
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 600,
      targetHeight: 300,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((data) => {
      this.visitorPic = "data:image/jpeg;base64," + data;
      this.isShowPic = true;
    }, (error) => {

    });
  }
  getIdPic() {
    this.camera.getPicture({
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 320,
      targetHeight: 320,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((data) => {
      this.idPic = "data:image/jpeg;base64," + data;
      this.isShowIdPic = true;
    }, (error) => {

    });
  }
  /* createThumbnail() {
    this.generateFromImage(this.idPic, 200, 200, 0.5, data => {
      this.isShowIdPic = data;
      this.smallSize = this.getImageSize(this.smallImg);
    });
  }
  generateFromImage(img, MAX_WIDTH: number = 700, MAX_HEIGHT: number = 700, quality: number = 1, callback) {
    var canvas: any = document.createElement("canvas");
    var image = new Image();
 
    image.onload = () => {
      var width = image.width;
      var height = image.height;
 
      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }
      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext("2d");
 
      ctx.drawImage(image, 0, 0, width, height);
 
      // IMPORTANT: 'jpeg' NOT 'jpg'
      var dataUrl = canvas.toDataURL('image/jpeg', quality);
 
      callback(dataUrl)
    }
    image.src = img;
  }
 
  getImageSize(data_url) {
    var head = 'data:image/jpeg;base64,';
    return ((data_url.length - head.length) * 3 / 4 / (1024*1024)).toFixed(4);
  } */
  addVisitor() {
    if (!this.isShowPic) {
      const toast = this.toastCtrl.create({
        message: 'Add the photo',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return false;
    }
    if (this.isShowIdPic == undefined) {
      const toast = this.toastCtrl.create({
        message: 'ID is required..',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    // Loading
    let loading = this.loadingCtrl.create({
      content: 'Processing ...'
    });
    loading.present();
    var body = {
      mainphoto: this.visitorPic,
      idcard: this.idPic,
      currentPhoto: null
    };
    if (this.name == "") {
      return this.isname = true;
    }
    if (this.existVis != null) {
      if (this.existVis.typeofVistor != this.category) {
        this.visId = '';
      }
    }
    // this.api.doAddVisitor(this.name, this.mobileNo, this.vehicleNo, this.companyName, this.selectedBlockNo, this.selectedBlockLabel, this.user['apartmentId'], this.visId, this.category, this.user['id'], body).then((res) => {
    //   loading.dismiss();
    //   const toast = this.toastCtrl.create({
    //     message: 'Visitor request placed for the approval..!',
    //     duration: 3000,
    //     position: 'bottom',
    //     cssClass: 'toast-green'
    //   });
    //   toast.present();
    // }, (error) => {
    //   this.error = error;
    // });
    this.navCtrl.push(GatehomePage);
    this.next = 1;
    this.ionViewDidLoad();
  }

  searchComapny() {
    // this.api.searchCompany(this.srcCompany, this.user['id']).then((res) => {
    //   const data = JSON.parse(res['_body']);
    //   this.companies = data;
    //   this.companyName = this.srcCompany;
    //   this.catCompany = this.getCompanies();
    //   console.log(this.catCompany);
    // })
  }
  addCompany() {
    // this.api.addCompany(this.companyName, 'Others', '', this.user['id']).then((res) => {
    // });
  }

  onClickBack() {
    if (this.next > 1) {
      this.next--;
    }
  }

  onClickNext() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: 'loading ...',
      duration: 1000
    });
    loading.onDidDismiss(() => {

    })
    loading.present(); 
    if (this.next == 1) {
      if (this.mobileNo == "" || Number(this.mobileNo.length) < 10) {
        return this.mob = true;
      }
      else {
        // this.api.getExistAllDetails(this.mobileNo, this.user['id']).then((res) => {
        //   const data = JSON.parse(res['_body']);
        //   this.existVis = data;
        //   if (this.existVis != undefined) {
        //     this.existVis = data.sort(function (a, b) {
        //       return parseFloat(b.id) - parseFloat(a.id);
        //     });
        //     this.existVis = data.reduce(
        //       (accumulator, current) => accumulator.some(x => x.typeofVistor === current.typeofVistor) ? accumulator : [...accumulator, current], []
        //     )
        //     /* let dat  = this.existVis;
        //    dat = Array.from(new Set(dat.map(i=>i.typeofVistor))).map((k, i)=> {
        //       return dat[i];
        //     });
        //     this.existVis = dat; */
        //     if (this.existVis[0].access != 'left' && this.existVis[0].access != 'declined' && this.existVis[0].access != 'created' && this.existVis[0].access != 'pending') {
        //       this.navCtrl.push(AllVisitorsListPage, { visitor: this.existVis });
        //     } else {
        //       if (this.cat_loop == 0) {
        //         for (let i = 0; i < this.categories.length; i++) {
        //           let j;
        //           loop1:
        //           for (j = 0; j < this.existVis.length; j++) {
        //             if (this.existVis[j].typeofVistor == this.categories[i].cat) break loop1;
        //           }
        //           if (j == this.existVis.length)
        //             this.resCat.push(this.categories[i]);
        //         }
        //         this.cat_loop++;
        //       }
        //       console.log(this.resCat);
        //       this.visId = this.existVis[0].id;
        //       this.vehicleNo = this.existVis[0].vehicleNo;
        //       this.name = this.existVis[0].name;
        //       this.idPic = this.existVis[0].idCard;
        //       console.log(this.existVis);
        //       this.next = 3;
        //     }
        //   }
        //   else {
        //     this.next = 2;
        //     this.triggerOTP();
        //     if (this.cat_loop == 0) {
        //       for (let i = 0; i < this.categories.length; i++) {
        //         this.resCat.push(this.categories[i]);
        //       }
        //       this.cat_loop++;
        //     }
        //   }
        // });
      }
    }
    if (this.next == 2) {
      this.onClickOtpVerify();
      if (this.wrong) {
        return;
      }
    }
    if (this.next == 3) {
      if (this.name == "") {
        return this.isname = true;
      } else {
        this.next++;
      }
    }
    if (this.next == 5) {
      if (this.companies.length == 0) {
        const confirm = this.alertCtrl.create({
          title: 'New Company',
          subTitle: 'Name: ' + this.companyName,
          buttons: [
            {
              text: 'Add',
              handler: () => {
                // this.api.addCompany(this.companyName, 'Others', '', this.user['id']).then((res) => {
                //   this.next++;
                // });
              }
            }
          ]
        });

        confirm.present();
        return;
      }
    }
  }

  onClickEvent(item) {
    if (item.typeofVistor == 'maid') {
      this.navCtrl.push(GateformPage, { maid: item });
      this.next--;
    }
    else if (item.typeofVistor == 'driver') {
      this.navCtrl.push(AdddriverPage, { driver: item });
      this.next--;
    }
    /* else if (item.typeofVistor == 'guest') {
      this.vehicleNo = item['vehicleNo'];
      this.name = item['name'];
      this.category = item['typeofVistor'];
      console.log(item);
      this.next+1;
    } */
    this.visId = item['id'];
    this.vehicleNo = item['vehicleNo'];
    this.name = item['name'];
    this.idPic = item['idCard'];
    this.category = item['typeofVistor']
    console.log(item);
    this.next++;
  }

  onClickType(item) {

    if (this.name == '') {
      return this.isname = true;
    }

    this.category = item.cat;
    console.log(this.category);
    this.next++;
  }

  onClickAddNewCompany() {
    if (this.companies.length == 0) {
      const confirm = this.alertCtrl.create({
        title: 'New Company',
        subTitle: 'Name: ' + this.companyName,
        buttons: [
          {
            text: 'Add',
            handler: () => {
              // this.api.addCompany(this.companyName, 'Others', '', this.user['id']).then((res) => {
              //   this.next++;
              // });
            }
          }
        ]
      });
      confirm.present();
      return;
    }
  }

  triggerOTP() {
    // this.api.triggerOTP(this.mobileNo, this.user['id']).then((res) => {
    //   this.rotp = res['_body'];
    //   console.log(this.rotp);
    // });
  }

  selectedCompany: any = { id: 0, name: '' };
  onClickCompany(company) {
    if (this.companies.length > 0) {
      this.selectedCompany = company;
      this.companyName = company.companyName;
    }
  }

  onClickBlock(block) {
    let curApId = this.user['apartmentId'];
    this.selectedBlock = block;
    this.blockNo = block.name;
    // this.api.getFlatDetails(this.selectedBlock['id'], curApId, this.user['id']).then((res) => {
    //   const data = JSON.parse(res['_body']);
    //   console.log(data);
    //   this.flats = data;
    // })

  }

  onClickFlat(flat) {
    let isflat = false;
    this.selectedFlat = flat;
    if (this.selectedBlocks.length <= 0) {
      this.flatNo = flat.flatName;
      const obj = { block: this.selectedBlock, flat: this.selectedFlat };
      this.selectedBlocks.push(obj);
      this.displayFlat();
    }
    else {
      for (let i of this.selectedBlocks) {
        if (flat.blockId == i.flat.blockId && flat.id == i.flat.id) {
          isflat = true;
          break;
        }
      }
      if (!isflat) {
        this.flatNo = flat.flatName;
        const obj = { block: this.selectedBlock, flat: this.selectedFlat };
        this.selectedBlocks.push(obj);
        this.displayFlat();
      }
    }
  }

  displayFlat() {
    let labelId = "";
    let labelName = "";
    if (this.selectedBlocks.length > 0) {
      for (let i = 0; i < this.selectedBlocks.length; i++) {
        if (i > 0) {
          labelName = `${labelName},`;
          labelId = `${labelId},`;
        }
        const curBlock = this.selectedBlocks[i].block;
        const curFlat = this.selectedBlocks[i].flat;
        labelId = labelId + curBlock.id + "-" + curFlat.id;
        labelName = labelName + curBlock.name + "-" + curFlat.flatName;
      }
    }
    this.selectedBlockNo = labelId;
    this.selectedBlockLabel = labelName;
  }

  onClickClose(idx) {
    this.selectedBlocks = this.selectedBlocks.filter((item, i) => {
      return idx !== i;
    });
    this.displayFlat();
  }

  onClickOtpVerify() {
    if (this.rotp === this.otp) {
      this.verify = true;
      this.wrong = false;
      this.otp = '';
      this.next = 3;
    } else {
      this.wrong = true;
    }
  }

  isCompany() {
    if (this.companyName == '') {
      this.iscompanies = true;
      const toast = this.toastCtrl.create({
        message: 'Please select any company.!',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    else {
      this.next++;
    }
  }

  isBlock() {
    if (this.blockNo == '' || this.flatNo == '') {
      this.isblock = true;
      const toast = this.toastCtrl.create({
        message: 'Please select  a flat.!',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return true;
    }
    else {
      if (this.existVis != undefined) {
        this.next = 6;
      }
      else {
        this.next++;
      }
    }
  }
}