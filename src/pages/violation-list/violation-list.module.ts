import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViolationListPage } from './violation-list';

@NgModule({
  declarations: [
    ViolationListPage,
  ],
  imports: [
    IonicPageModule.forChild(ViolationListPage),
  ],
})
export class ViolationListPageModule {}
