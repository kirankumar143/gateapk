import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AddViolationPage } from '../add-violation/add-violation';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the ViolationListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-violation-list',
  templateUrl: 'violation-list.html',
})
export class ViolationListPage {
  user : {};
  apartmentId: string;
  srcViolation ="";
  violationList : any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage: Storage,public api: ApiProvider) {
    this.storage.get('user').then((val) => {
      this.user = (val);
    });
  }

  ionViewDidEnter() {
    
      this.apartmentId = this.user["apartmentId"];
      // this.api.getViolationList(this.user["apartmentId"],'10','10000',this.srcViolation,this.user['id']).then(res => {
      //   const data = JSON.parse(res['_body']);
      //   console.log(data);
      //   if(data==null){
      //     this.violationList = 0;
      //   }else{
      //     this.violationList = data;
      //     console.log(data);
      //   }
      // })
  }
  getClass(i) {
    if (i % 2 == 0) {
      return 'bg-green';
    }
    else if (i % 3 == 0) {
      return 'bg-red';
    }
    else if (i % 4 == 0) {
      return 'bg-blue';
    }
    else if (i % 5 == 0) {
      return 'bg-goldenrod';
    }
    else {
      return 'bg-purple';
    }
  }
  searchViolations(){
    // this.api.getViolationList(this.user["apartmentId"],'10','10000',this.srcViolation,this.user['id']).then(res => {
    //   const data = JSON.parse(res['_body']);
    //   this.violationList=data;
    // })
  }
  onClickAddViolation(){
    this.navCtrl.push(AddViolationPage);
  }
  doRefresh(refresher){
    this.ionViewDidEnter();
    refresher.complete();
  }
}
