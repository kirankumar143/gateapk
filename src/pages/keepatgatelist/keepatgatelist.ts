import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { KagPopoverComponent } from '../../components/kag-popover/kag-popover';

/**
 * Generated class for the KeepatgatelistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-keepatgatelist',
  templateUrl: 'keepatgatelist.html',
})
export class KeepatgatelistPage {

  user: {};
  KAGList: any;
  search = "";

  constructor(
    public popoverCtrl: PopoverController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public storage: Storage
  ) {
  }

  ionViewWillEnter(){
    this.storage.get('user').then(val =>{
      this.user = (val);
      // this.api.getKAGList(this.user['apartmentId'],this.user['id'],this.search).then(res =>{
      //   const data = JSON.parse(res['_body']);
      //   if(data!=null){
      //     this.KAGList = data;
      //   }
      //   else{
      //     this.KAGList = 0;
      //   }
      //   console.log(this.KAGList);
      // })
    })
  }

  doRefresh(refresher){
    this.ionViewWillEnter();
    refresher.complete();
  }

  getClass(i){
    if(i%2==0){
      return 'bg-green';
    }
    else if(i%3==0){
      return 'bg-red';
    }
    else if(i%4==0){
      return 'bg-blue';
    }
    else if(i%5==0){
      return 'bg-goldenrod';
    }
    else{
      return 'bg-purple';
    }
  }
  onClickIssueDelivery(item){
    const popover = this.popoverCtrl.create(KagPopoverComponent, {item: item});
    popover.present();

    popover.onDidDismiss(()=>{
      this.ionViewWillEnter();
    })
  }
  searchVis(){
    this.storage.get('user').then(val =>{
      this.user = (val);
      // this.api.getKAGList(this.user['apartmentId'],this.user['id'],this.search).then(res =>{
      //   this.KAGList = JSON.parse(res['_body']);
      // })
    })
  }
}
