import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KeepatgatelistPage } from './keepatgatelist';

@NgModule({
  declarations: [
    KeepatgatelistPage,
  ],
  imports: [
    IonicPageModule.forChild(KeepatgatelistPage),
  ],
})
export class KeepatgatelistPageModule {}
