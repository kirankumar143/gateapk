import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';
import { VisdetailsPage } from '../visdetails/visdetails';

/**
 * Generated class for the OtherslistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otherslist',
  templateUrl: 'otherslist.html',
})
export class OtherslistPage {
  user :string;
  apartmentId :string;
  othersList : any;
  srcVis= '';
  isScroll = true;
  wait: number = 1;
  page = false;
  isData = false;

  constructor(public alertCtrl:AlertController,public navCtrl: NavController, public navParams: NavParams,public storage:Storage,public api :ApiProvider) {
  }

  ionViewWillEnter() {
    // Loading
    this.storage.get('user').then((val) => {
      this.user = (val);
      this.apartmentId = this.user["apartmentId"];
      // this.api.getVisitorsList(this.apartmentId,'10000',this.srcVis,'others',this.user['id']).subscribe((res) =>{
      //   const data = JSON.parse(res['_body']);
      //   console.log(data);
      //   this.page = true;
      //   console.log(data);
      // if(data.list !== null){
      // this.vlistUpdate(data.list);
      // this.page = false;
      // console.log(data);
      // this.onWait();
      // }
      // else{
      //   this.isData = true;
      //   this.page = false;
      // }
      // });
    });
  }

  vlistUpdate(vlist) {
    this.othersList = vlist.map(dat => {
      dat.plotName = dat.plotName.split(',');
      dat.blockNo = dat.blockNo.split(',').map((flt, index) => {
        flt = flt.split('/');
        return { name: dat.plotName[index], status: flt[1] };
      });
      return dat;
    });
  }

  doRefresh(refresher){
    this.ionViewWillEnter();
    refresher.complete();
  }

  doScrolling(infinite) {
    if(this.othersList!=null){
    const tmp = this.othersList[this.othersList.length - 1];

    setTimeout(() => {
      this.isScroll = false;
      // this.api.getVisitorsList(this.apartmentId, tmp['id'], this.srcVis,'others',this.user['id']).subscribe((res) => {
      //   const data = JSON.parse(res['_body']);
      //   if (data.list !== null) {
      //     data.list = data.list.map(dat => {
      //       console.log(dat);
      //       dat.plotName = dat.plotName.split(',');
      //       dat.blockNo = dat.blockNo.split(',').map((flt, index) => {
      //         flt = flt.split('/');
      //         return { name: dat.plotName[index], status: flt[1] };
      //       });
      //       return dat;
      //     });
      //     this.othersList = [...this.othersList, ...data.list];
      //   }
      //   this.isScroll = true;
      // });
    }, 1000);
  }
}
  searchVis() {
    // this.api.getVisitorsList(this.apartmentId, '10000', this.srcVis,'others',this.user['id']).subscribe((res) => {
    //   const data = JSON.parse(res['_body']);
    //   if(data.list!=null){
    //     this.othersList = data.list.map(dat => {
    //       dat.plotName = dat.plotName.split(',');
    //       dat.blockNo = dat.blockNo.split(',').map((flt, index) => {
    //         flt = flt.split('/');
    //         return { name: dat.plotName[index], status: flt[1] };
    //       });
    //       return dat;
    //     });
    //   }
    //   else{
    //     this.othersList = 0;
    //   }
    // });
  }

  onClickAllow(item) {
    event.stopPropagation();
    const confirm = this.alertCtrl.create({
      title: 'Issueing In Gate Pass',
      subTitle: 'Name: ' + item.name,
      buttons: [
        {
          text: 'Issue In Pass',
          handler: () => {
            // this.api.allowVisitor(item.vistorId, item.rId, this.user['username'],'others',item.plotName,this.user['id']).then(res =>{
            //   this.ionViewWillEnter();
            // });
          }
        }
      ]
    });

    confirm.present();
  }

  onClickOut(item) {
    event.stopPropagation();
    const confirm = this.alertCtrl.create({
      title: 'Issueing Out Gate Pass',
      subTitle: 'Name: ' + item.name,
      buttons: [
        {
          text: 'Issue Out Pass',
          handler: () => {
            // this.api.onOut(item.vistorId, this.user['apartmentId'], item.plotName, item.rId,this.user['id']).then((res)=>{
            //   this.ionViewWillEnter();
            // });
          }
        }
      ]
    });

    confirm.present();
  }
  onWait() {
    setTimeout(() => {
      if (this.wait <= 3) {
        this.wait++;
        this.onWait();
      }
      return
    }, 7000);
  }

  onClickArrow(event, item){
    event.stopPropagation();
    item.id = item.vistorId;
    this.navCtrl.push(VisdetailsPage, { item: item });
  }
}

