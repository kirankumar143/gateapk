import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KeepatgatetabPage } from './keepatgatetab';

@NgModule({
  declarations: [
    KeepatgatetabPage,
  ],
  imports: [
    IonicPageModule.forChild(KeepatgatetabPage),
  ],
})
export class KeepatgatetabPageModule {}
