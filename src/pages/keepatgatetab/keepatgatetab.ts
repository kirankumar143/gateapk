import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { KeepatgatelistPage } from '../keepatgatelist/keepatgatelist';
import { IssueddellistPage } from '../issueddellist/issueddellist';

/**
 * Generated class for the KeepatgatetabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-keepatgatetab',
  templateUrl: 'keepatgatetab.html',
})
export class KeepatgatetabPage {

  tab1Root = KeepatgatelistPage;
  tab2Root = IssueddellistPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KeepatgatetabPage');
  }

}
