import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { VisdetailsPage } from '../visdetails/visdetails';
/**
 * Generated class for the VisitorlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-visitorlist',
  templateUrl: 'visitorlist.html',
})
export class VisitorlistPage {

  vlist: any = [];
  user: {};
  wait: number = 1;
  apartmentId: string;
  isScroll = true;
  srcVis = "";
  lsize = 0;
  page = false;
  isData = false;
  pollStart = false;
  existVis: any = [];
  constructor(
    public alertCtrl: AlertController,
    public api: ApiProvider,
    public storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
  ) {
    this.existVis = this.navParams.get('mobile');

  }

  ionViewWillEnter() {
    // Loading
    this.storage.get('user').then((val) => {
      this.user = (val);
      if (this.existVis) {
        this.vlist.push(this.existVis);
        console.log(this.vlist);
      } else {
        // this.api.getVisitorsList(this.user["apartmentId"], '10000', this.srcVis, 'vistor', this.user['id']).subscribe((res) => {
        //   const data = JSON.parse(res['_body']);
        //   this.page = true;
        //   if (data.list !== null) {
        //     this.vlistUpdate(data.list);
        //     console.log(data);
        //     this.page = false;
        //     this.onWait();
        //   }
        //   else {
        //     this.vlist = 0;
        //     this.isData = true;
        //     this.page = false;
        //   }
        // });
      }
    });
  }

  vlistUpdate(vlist) {
    if (this.existVis) {
      this.existVis = vlist.map(dat => {
        console.log(dat);
        dat.plotName = dat.plotName.split(',');
        dat.blockNo = dat.blockNo.split(',').map((flt, index) => {
          flt = flt.split('/');
          const numbers=flt[0].split('-');
          return { name: `${numbers[0]}-${numbers[1]}`, status:flt[1] };
      
          /* return { name: dat.plotName[index], status: flt[1] }; */
        });
        dat.blockNo = Array.from(new Set(dat.blockNo.map(i=>i.name))).map((k,i)=>{
          return{name: dat.plotName[i], status: dat.blockNo.find(f=>f.name===k).status };
        });
        return dat;
      });
    }
    else {
      this.vlist = vlist.map(dat => {
        console.log(dat);
        dat.plotName = dat.plotName.split(',');
        dat.blockNo = dat.blockNo.split(',').map((flt, index) => {
          flt = flt.split('/');

          const numbers=flt[0].split('-');
          return { name: `${numbers[0]}-${numbers[1]}`, status: flt[1] };
        });
        dat.blockNo = Array.from(new Set(dat.blockNo.map(i=>i.name))).map((k, i)=> {
          return {name: dat.plotName[i], status: dat.blockNo.find(f=>f.name===k).status };
        });

        return dat;
      });
    }
  }

  doRefresh(refresher) {
    this.ionViewWillEnter();
    refresher.complete();
  }

  doScrolling(infinite) {
    if (this.vlist != null) {
      const tmp = this.vlist[this.vlist.length - 1];
      setTimeout(() => {
        this.isScroll = false;
        // this.api.getVisitorsList(this.apartmentId, tmp['id'], this.srcVis, 'vistor', this.user['id']).subscribe((res) => {
        //   const data = JSON.parse(res['_body']);
        //   if (data.list !== null) {
        //     data.list = data.list.map(dat => {
        //       console.log(dat);
        //       dat.plotName = dat.plotName.split(',');
        //       dat.blockNo = dat.blockNo.split(',').map((flt, index) => {
        //         flt = flt.split('/');
        //         return { name: dat.plotName[index], status: flt[1] };
        //       });
        //       return dat;
        //     });
        //     this.vlist = [...this.vlist, ...data.list];
        //   }
        //   this.isScroll = true;
        // });
      }, 1000);
    }
  }
  searchVis() {
    // this.api.getVisitorsList(this.user['apartmentId'], '10000', this.srcVis, 'vistor', this.user['id']).subscribe((res) => {
    //   const data = JSON.parse(res['_body']);
    //   console.log(data);
    //   if (data.list != null) {
    //     this.vlist = data.list.map(dat => {
    //       dat.plotName = dat.plotName.split(',');
    //       dat.blockNo = dat.blockNo.split(',').map((flt, index) => {
    //         flt = flt.split('/');
    //         return { name: dat.plotName[index], status: flt[1] };
    //       });
    //       return dat;
    //     });
    //   }
    //   else {
    //     this.vlist = 0;
    //   }
    // });
  }

  onClickAllow(item) {
    event.stopPropagation();
    const confirm = this.alertCtrl.create({
      title: 'Issueing In Gate Pass',
      subTitle: 'Name: ' + item.name,
      buttons: [
        {
          text: 'Issue In Pass',
          handler: () => {
            let flats: string = '';
            for (let i of item.plotName) {
              flats += i + ',';
            }
            // this.api.allowVisitor(item.vistorId, item.rId, this.user['username'], 'vistor', flats, this.user['id']).then(res => {
            //   this.ionViewWillEnter();
            // });
          }
        }
      ]
    });
    confirm.present();
  }

  onClickOut(item) {
    /* let date = new Date(parseInt(item.inTime)).toISOString().substring(0,10);
    let time = new Date(parseInt(item.inTime)).toISOString().substring(11,19);
    let data = date+' '+time; */
    event.stopPropagation();
    const confirm = this.alertCtrl.create({
      title: 'Issueing Out Gate Pass',
      subTitle: 'Name: ' + item.name,
      buttons: [
        {
          text: 'Issue Out Pass',
          handler: () => {
            let flats: string = '';
            for (let i of item.plotName) {
              flats += i + ',';
            }
            // this.api.onOut(item.vistorId, this.user['apartmentId'], flats, item.rId, this.user['id']).then((res) => {

            //   this.ionViewWillEnter();
            // });
          }
        }
      ]
    });

    confirm.present();
  }

  pushWait(data) {
    return data.map((d) => {
      return { ...d, wait: 1 }
    });
  }

  onWait() {
    setTimeout(() => {
      if (this.wait <= 3) {
        this.wait++;
        this.onWait();
      }
      return
    }, 7000);
  }

  onClickArrow(event, item) {
    event.stopPropagation();
    item.id = item.vistorId;
    this.navCtrl.push(VisdetailsPage, { item: item });
  }
  getClass(i) {
    if (i % 2 == 0) {
      return 'bg-green';
    }
    else if (i % 3 == 0) {
      return 'bg-red';
    }
    else if (i % 4 == 0) {
      return 'bg-blue';
    }
    else if (i % 5 == 0) {
      return 'bg-goldenrod';
    }
    else {
      return 'bg-purple';
    }
  }
}
