import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DriverlistPage } from '../driverlist/driverlist';
import { AdddriverPage } from '../adddriver/adddriver';

/**
 * Generated class for the DrivertabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-drivertab',
  templateUrl: 'drivertab.html',
})
export class DrivertabPage {

  constructor(public storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
  }

  user: {};

  ionViewDidLoad() {
    this.storage.get('user').then((val) =>{
      this.user = (val);
    });
  }

  tab1Root = AdddriverPage;
  tab2Root = DriverlistPage;

}
