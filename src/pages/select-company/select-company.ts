import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController, ViewController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-select-company',
  templateUrl: 'select-company.html',
})
export class SelectCompanyPage {
  srcCompany: string;
  user: {};
  companies = [];
  companyName = "";
  catCompany: any[] = [];
  cat_loop: number;
  next: number = 1;
  name = "";
  mobileNo = "";
  vehicleNo: string;
  selectedBlock = {};
  selectedFlat = {};
  selectedBlocks = [];
  flats = [];
  visitorPic = "";
  idPic = "";
  isShowPic = false;
  isShowIdPic = false;
  blocks = [];





  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public api: ApiProvider,
    public alertCtrl: AlertController,
    public storage: Storage,
    public viewCtrl:ViewController


    ) {
  }

  ionViewDidLoad() {
    this.cat_loop = 0;
    this.name = ""; this.mobileNo = ""; this.vehicleNo = ""; this.srcCompany = ""; this.selectedBlocks = []; this.flats = []; this.companyName = ""; this.visitorPic = ''; this.idPic = '';
    this.selectedCompany = { id: 0, name: '' }; this.selectedBlock = {}; this.selectedFlat = {}; this.isShowIdPic = false; this.isShowPic = false;

    this.storage.get('user').then((val) => {
      this.user = (val);
      // this.api.getBlockDetails(this.user['apartmentId'], this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   this.blocks = data;
      // });

      // this.api.getCompanyDetails(this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   this.companies = data;
      //   this.catCompany = this.getCompanies();
      // })
    });
  
  }
  public onClickCancel() {
    this.viewCtrl.dismiss();
  }
  selectedCompany: any = { id: 0, name: '' };
  onClickCompany(company) {
    if (this.companies.length > 0) {
      this.selectedCompany = company;
      this.companyName = company.companyName;
      this.viewCtrl.dismiss(this.companyName);
    }
  }
  onClickAddNewCompany() {
    if (this.companies.length == 0) {
      const confirm = this.alertCtrl.create({
        title: 'New Company',
        subTitle: 'Name: ' + this.companyName,
        buttons: [
          {
            text: 'Add',
            handler: () => {
              // this.api.addCompany(this.companyName, 'Others', '', this.user['id']).then((res) => {
              //   this.next++;
              // });
            }
          }
        ]
      });
      confirm.present();
      return;
    }
  }

  searchComapny(){
    // this.api.searchCompany(this.srcCompany, this.user['id']).then((res) => {
    //   const data = JSON.parse(res['_body']);
    //   this.companies = data;
    //   this.companyName = this.srcCompany;
    //   this.catCompany = this.getCompanies();
    //   console.log(this.catCompany);
    // })
  }
  getCompanies() {
    let c = '';
    let cc: any[] = [];

    for (let i = 0; i < this.companies.length; i++) {
      if (c !== this.companies[i].companyCategory) {
        let obj = { id: this.companies[i].id, cat: this.companies[i].companyCategory, d: [this.companies[i]] };
        cc.push(obj);
        c = this.companies[i].companyCategory;
      } else {
        cc[cc.length - 1].d.push(this.companies[i]);
      }
    }
    return cc;
  }
}
