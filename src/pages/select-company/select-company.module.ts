import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectCompanyPage } from './select-company';

@NgModule({
  declarations: [
    SelectCompanyPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectCompanyPage),
  ],
})
export class SelectCompanyPageModule {}
