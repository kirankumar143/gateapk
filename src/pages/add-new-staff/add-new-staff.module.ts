import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddNewStaffPage } from './add-new-staff';

@NgModule({
  declarations: [
    AddNewStaffPage,
  ],
  imports: [
    IonicPageModule.forChild(AddNewStaffPage),
  ],
})
export class AddNewStaffPageModule {}
