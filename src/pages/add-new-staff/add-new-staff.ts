import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';

/**
 * Generated class for the AddNewStaffPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-new-staff',
  templateUrl: 'add-new-staff.html',
})
export class AddNewStaffPage {
  next: number=1;
  visitorPic = "";
  notshow: boolean;
  isShowPic = false;
  show: boolean;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public camera: Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddNewStaffPage');
  }
  onClickNext() {

    if (this.next == 1) {
      this.next = 2;
    }
    if (this.next == 2) {
      this.notshow = true;

     }
    if (this.next == 3) {

    }
    if (this.next == 4) {
     
    }

  }
  getPic() {
    this.camera.getPicture({
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 600,
      targetHeight: 300,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((data) => {
      this.visitorPic = "data:image/jpeg;base64," + data;
      this.isShowPic = true;
    }, (error) => {

    });
  }
  gototheaddpicture() {

    this.show = true;
    this.notshow = false;


  }

}
