import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import { AddbusPage } from '../addbus/addbus';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { VisdetailsPage } from '../visdetails/visdetails';
import { EditbusPage } from '../editbus/editbus';

/**
 * Generated class for the BusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bus',
  templateUrl: 'bus.html',
})
export class BusPage {

  busList: any;
  user: {};
  isArrive=false;
  isLeft=true;
  busId: string;
  selBus: any []=[];
  item = [];
  page = false;
  isData = false;
  search = "";

  constructor(
    public alertCtrl: AlertController,
    public api: ApiProvider,
    public storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    ) {
  }

  ionViewWillEnter() {
    this.storage.get('user').then((val) => {
      this.user = (val);
      // this.api.getSchoolbusList(this.user["apartmentId"],this.user['id'],this.search).subscribe((res) =>{
      //   const data = JSON.parse(res['_body']);
      //   this.page = true;
      //   console.log(data);
      //   if(data !== null){
      //     this.busList = data;
      //     this.page = false;
      //   }
      //   else{
      //     this.isData = true;
      //     this.page = false;
      //   }
      //   });
    });
  }

  doRefresh(refresher) {
    this.ionViewWillEnter();
    refresher.complete();
  }

  onClickArriveBus(item){
    event.stopPropagation();
      const confirm = this.alertCtrl.create({
        title: 'Are you sure..!',
        subTitle: 'about '+'<font color="green" size="4">Arrival</font> of the bus',
        buttons: [
          {
            text: 'Cancel',
            handler: () => {
            }
          },
          {
            text: 'Yes',
            handler: () => {
              // this.api.updateBusIn(item.id,item.blockNumber,this.user['apartmentId'],item.plotNumber,this.user['id']).then((res)=>{
              //   item.access="arrived";
              //   this.ionViewWillEnter();
              // });
            }
          }
         ]
      });
      confirm.present();
  }

  onClickLeftBus(item){
    event.stopPropagation();
    const confirm = this.alertCtrl.create({
      title: 'Are you sure..!',
      subTitle: 'about '+'<font color="red" size="4">Left</font> of the bus',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            // this.api.onOut(item.id, this.user['apartmentId'], item.plotNumber, item.rId,this.user['id']).then((res)=>{
            //   item.access="left";
            //   this.ionViewWillEnter();
            // });
          }
        }
       ]
    });
    confirm.present();
  }

  onClickAddBus(){
    this.navCtrl.push(AddbusPage);
  }
  onClickupdateBus(bus){
    this.navCtrl.push(EditbusPage,{bus:bus});
  }
  onClickArrow($event,item){
    event.stopPropagation();
    this.navCtrl.push(VisdetailsPage, { item:item });
  }
  getClasss(i){
    if(i%2==0){
      return 'bg-green';
    }
    else if(i%3==0){
      return 'bg-red';
    }
    else if(i%4==0){
      return 'bg-blue';
    }
    else if(i%5==0){
      return 'bg-goldenrod';
    }
    else{
      return 'bg-purple';
    }
  }
  searchBusList(){
      // this.api.getSchoolbusList(this.user["apartmentId"],this.user['id'],this.search).subscribe((res) =>{
      //   const data = JSON.parse(res['_body']);
      //   this.busList=data;
      // })
  }
}
