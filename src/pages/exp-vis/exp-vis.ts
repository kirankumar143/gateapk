import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, PopoverController, LoadingController} from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { VisdetailsPage } from '../visdetails/visdetails';
import { ExpvisLoginPopupComponent } from '../../components/expvis-login-popup/expvis-login-popup';

/**
 * Generated class for the ExpVisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-exp-vis',
  templateUrl: 'exp-vis.html',
})
export class ExpVisPage {

  user: {};
  expVisList: any;
  search: string='';

  constructor(public popoverCtrl: PopoverController,public alertCtrl: AlertController,public api: ApiProvider,public storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.storage.get('user').then(val=>{
      this.user = (val);

      // this.api.getExpVisList(this.user['apartmentId'],this.user['id'], this.search).then(res =>{
      //   const data = JSON.parse(res['_body']);
      //   console.log(data);
      //   if(data!=null){
      //     this.expVisList = data;
      //   }
      //   else{
      //     this.expVisList = 0;
      //   }
      // });
    });

  }

  onClickAllow(item){
    event.stopPropagation();
    const popover = this.popoverCtrl.create(ExpvisLoginPopupComponent, {item: item});
    popover.present();

    popover.onDidDismiss(data =>{
      this.ionViewDidLoad();
    })
  }

  doRefresh(refresher) {
    this.ionViewDidLoad();
    refresher.complete();
  }

  srcVis(){
    // this.api.getExpVisList(this.user['apartmentId'],this.user['id'], this.search).then(res =>{
    //   const data = JSON.parse(res['_body']);
    //   console.log(data);
    //   this.expVisList = data;
    // });
  }

  /* onClickDeny(item){
    event.stopPropagation();
    const confirm = this.alertCtrl.create({
      title: 'Are you sure..!',
      subTitle: 'to '+'<font color="red" size="4">Reject</font> the entry of '+'<br><b>'+item.name+'</b>',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.api.expVisOut(item.id, item.apartmentId, item.plotNumber);
          }
        }
       ]
    });
    confirm.present();

  }
 */

getClass(i){
  if(i%2==0){
    return 'bg-green';
  }
  else if(i%3==0){
    return 'bg-red';
  }
  else{
    return 'bg-blue';
  }
}

  onClickArrow(event, item){
    event.stopPropagation();
    this.navCtrl.push(VisdetailsPage, { item: item });
  }

}
