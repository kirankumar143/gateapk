import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OtherslistPage } from '../otherslist/otherslist';
import { AddothersPage } from '../addothers/addothers';

/**
 * Generated class for the OtherstabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otherstab',
  templateUrl: 'otherstab.html',
})
export class OtherstabPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }
  tab1Root = AddothersPage;
  tab2Root = OtherslistPage;
}
