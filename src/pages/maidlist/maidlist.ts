import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';
import { AlertController } from 'ionic-angular';
import { VisdetailsPage } from '../visdetails/visdetails';

/**
 * Generated class for the MaidlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
   */

@IonicPage()
@Component({
  selector: 'page-maidlist',
  templateUrl: 'maidlist.html',
})
export class MaidlistPage{

  user: {};
  apartmentId: string;
  maidList: any;
  maidId: string;
  maidStatus: string;
  srcVis = '';
  isScroll=true;
  page = false;
  isData = false;

  constructor(
    public alertCtrl: AlertController,
    public api: ApiProvider,
    public storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    ) { }

  ionViewWillEnter() {
    this.storage.get('user').then((val) => {
      this.user = (val);
      this.apartmentId = this.user["apartmentId"];
      // this.api.getVisitorsList(this.apartmentId,'10000',this.srcVis,'maid',this.user['id']).subscribe((res) =>{
      // const data = JSON.parse(res['_body']);
      // console.log(data);
      // this.page = true;
      // if(data.list !== null){
      // this.maidList = data.list;
      // this.page = false;
      // }
      // else{
      //   this.isData = true;
      //   this.page = false;
      // }
      // });
    });
  }

  doRefresh(refresher){
    this.ionViewWillEnter();
    refresher.complete();
  }
  doScrolling(infinite){

    const tmp = this.maidList[this.maidList.length-1];
    setTimeout(()=>{
      this.isScroll=false;
      // this.api.getVisitorsList(this.apartmentId,tmp['id'],this.srcVis,'maid',this.user['id']).subscribe((res) =>{
      //    const data = JSON.parse(res['_body']);
      //    if(data.list !== null) {
      //      this.maidList = [...this.maidList, ...data.list];
      //    }
      //    this.isScroll=true;
      //  });
    },1000);
  }

  searchVis(){
    // this.api.getVisitorsList(this.apartmentId,'10000',this.srcVis,'maid',this.user['id']).subscribe((res) =>{
    //    const data = JSON.parse(res['_body']);
    //    if(data.list==null){
    //     this.maidList = 0;
    //    }
    //    else{
    //      this.maidList = data.list;
    //    }
    //  });
 }

  onClickOut(item) {
    event.stopPropagation();

    const confirm = this.alertCtrl.create({
      title: 'Are you sure..!',
      subTitle: 'to '+'<font color="green" size="4">Out</font> the entry of '+'<br><b>'+item.name+'</b>',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Issue Out Pass',
          handler: () => {
            // this.api.onOut(item.vistorId,this.user['apartmentId'], item.plotName, item.rId,this.user['id']).then((res) =>{
            //   this.ionViewWillEnter();
            // });
          }
        }
       ]
    });
    confirm.present();
  }

  onClickArrow(event, item){
    event.stopPropagation();
    item.id = item.vistorId;
    this.navCtrl.push(VisdetailsPage, { item: item });
  }


}
