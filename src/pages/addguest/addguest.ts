import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api'
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';

/**
 * Generated class for the AddguestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addguest',
  templateUrl: 'addguest.html',
})
export class AddguestPage {

  mobileNo = '';
  name: string;
  vehicleNo: string;
  blockNo: string;
  flatNo: string;
  zone: string;
  agencyName: string;
  photoURL: string;
  next: number = 1;
  isDetails = true;
  selectedBlocks: any = [];
  user: {};
  selected: {};
  visitorPic = '';
  isShowPic = false;
  isShowPerson = true;
  isEnabled = true;
  blocks = [];
  flats = [];
  verify = false;
  isShowPersonPic = true;
  error: any;
  selectedBlockLabel: string;
  selectedBlockNo: string;
  selectedFlat: any = {};
  wrong = false;
  isMobile = false;
  isname = false;
  srcVis = "";
  visId = '';
  guest: string;
  constructor(public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public api: ApiProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public camera: Camera,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    this.guest = this.navParams.get('guest');
    console.log(this.guest);
    this.storage.get('user').then((val) => {
      this.user = (val);

      // this.api.getBlockDetails(this.user['apartmentId'], this.user['id']).then((res) => {
      //   const data = JSON.parse(res['_body']);
      //   this.blocks = data;
      // });
    });

  }

  getPic() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 320,
      targetHeight: 320
    }).then((data) => {
      this.visitorPic = "data:image/jpeg;base64," + data;
      this.isShowPic = true;
      this.isShowPerson = false;

    }, (error) => {

    });
  }

  addGuest() {
    if (!this.isShowPic) {
      const toast = this.toastCtrl.create({
        message: 'Add the photo',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-bg'
      });
      toast.present();
      return false;
    }
    // Loading
    let loading = this.loadingCtrl.create({
      content: 'Processing ...'
    });
    loading.present();
    var body = {
      mainphoto: this.visitorPic,
      idcard: null,
      currentPhoto: null
    };
    // this.api.addGuest(this.name, this.mobileNo, this.selectedBlockNo, this.selectedBlockLabel, this.user['apartmentId'], this.visId, this.user['id'], body).subscribe((res) => {
    //   loading.dismiss();
    //   const toast = this.toastCtrl.create({
    //     message: 'New Guest is added successfully.!',
    //     duration: 3000,
    //     position: 'bottom',
    //     cssClass: 'toast-green'
    //   });
    //   toast.present();
    // }, (error) => {
    //   this.error = error;
    // });
    this.next = 1;
    this.name = ""; this.mobileNo = ""; this.vehicleNo = ""; this.selectedBlocks = "";

    this.navCtrl.parent.select(1);
  }

  onClickBack() {
    if (this.next > 1) {
      this.next--;
    }
  }

  onClickNext() {

    if (this.next == 1) {
      if (this.mobileNo == "" || Number(this.mobileNo.length) < 10) {
        return this.isMobile = true;
      }
      if (!this.name) {
        return this.isname = true;
      }
    }
    if (this.next == 2) {
      if (this.blockNo == undefined || this.flatNo == undefined) {
        const toast = this.toastCtrl.create({
          message: 'Please select  a flat.!',
          duration: 3000,
          position: 'bottom',
          cssClass: 'toast-bg'
        });
        toast.present();
        return true;
      }
    }
    if (this.next == 3) {

    }
    this.next++;
  }

  selectedBlock: any = {}
  onClickBlock(block) {
    let curApId = this.user['apartmentId'];
    this.selectedBlock = block;
    this.blockNo = block.name;
    // this.api.getFlatDetails(this.selectedBlock['id'], curApId, this.user['id']).then((res) => {
    //   const data = JSON.parse(res['_body']);
    //   this.flats = data;
    // })
  }

  onClickFlat(flat) {
    let isflat = false;
    this.selectedFlat = flat;
    if (this.selectedBlocks.length <= 0) {
      this.flatNo = flat.flatName;
      const obj = { block: this.selectedBlock, flat: this.selectedFlat };
      this.selectedBlocks.push(obj);
      this.displayFlat();
    }
    else {
      for (let i of this.selectedBlocks) {
        if (flat.blockId == i.flat.blockId && flat.id == i.flat.id) {
          isflat = true;
          break;
        }
      }
      if (!isflat) {
        this.flatNo = flat.flatName;
        const obj = { block: this.selectedBlock, flat: this.selectedFlat };
        this.selectedBlocks.push(obj);
        this.displayFlat();
      }
    }
  }

  displayFlat() {
    let labelId = "";
    let labelName = "";
    if (this.selectedBlocks.length > 0) {
      for (let i = 0; i < this.selectedBlocks.length; i++) {
        if (i > 0) {
          labelName = `${labelName},`;
          labelId = `${labelId},`;
        }
        const curBlock = this.selectedBlocks[i].block;
        const curFlat = this.selectedBlocks[i].flat;
        labelId = labelId + curBlock.id + "-" + curFlat.id;
        labelName = labelName + curBlock.name + "-" + curFlat.flatName;
      }
    }
    this.selectedBlockNo = labelId;
    this.selectedBlockLabel = labelName;
  }

  onClickClose(idx) {
    this.selectedBlocks = this.selectedBlocks.filter((item, i) => {
      return idx !== i;
    });
    this.displayFlat();
  }

}
