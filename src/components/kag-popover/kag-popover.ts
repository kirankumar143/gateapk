import { Component } from '@angular/core';
import { ApiProvider } from '../../providers/api/api';
import { ViewController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the KagPopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'kag-popover',
  templateUrl: 'kag-popover.html'
})
export class KagPopoverComponent {

  user: {};
  item: {};
  pin: number;
  isPin = false;
  name: string;
  isName = false;

  constructor(
    public storage: Storage,
    public api: ApiProvider,
    public viewCtrl: ViewController,
    public navParams: NavParams
  ) {
    this.storage.get('user').then(val =>{
      this.user = (val);
    })
    this.item = this.navParams.get('item');
  }

  onClickAllow(){
    if(!this.name){
      return this.isName = true;
    }
    if(this.pin == this.item['assets']){
      // this.api.doRecievedDelivery(this.item['vistorId'], this.item['flatrId'],this.item['plotName'],this.user['username']+' - '+this.name,this.user['id']).then(res => {
      //   this.viewCtrl.dismiss();
      // });
    }else{
      this.isPin = true;
    }
  }

  onClickCancel(){
    this.viewCtrl.dismiss();
  }

}
