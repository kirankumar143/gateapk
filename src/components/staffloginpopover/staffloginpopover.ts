import { Component } from '@angular/core';
import { ApiProvider } from '../../providers/api/api';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { Camera } from '@ionic-native/camera';
import {  ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the StaffloginpopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'staffloginpopover',
  templateUrl: 'staffloginpopover.html'
})
export class StaffloginpopoverComponent {

  isShowPic=false;
  currentPhoto: string;
  staff: string;
  user: any;
  rId = '';
  srId: string;
  selected = false;

  constructor(public storage :Storage,public  navParams: NavParams,public camera: Camera,public api: ApiProvider,public viewCtrl:ViewController) {
    this.storage.get('user').then((val) => {
      this.user = (val);
    });
    this.staff = this.navParams.get('item');
  }

  onClickgetpic(item){

    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 320,
      targetHeight: 320
    }).then((data) => {
     this.currentPhoto = "data:image/jpeg;base64," + data;
     this.isShowPic = true;
     }, (error) => {

  });
  }

  onClickLogin(){
    // this.api.dayIn(this.staff['id'],this.user["username"],this.user['id']).then((res)=>{
    //   const data = JSON.parse(res['_body']);
    //   this.staff['rId'] = data;
    //   this.staff['inTime'] = new Date();
    //   this.staff['access'] = 'allowed';
    // });
    this.viewCtrl.dismiss(this.rId);

  }

}
