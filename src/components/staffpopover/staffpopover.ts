import { Component } from '@angular/core';
import { ApiProvider } from '../../providers/api/api';
import { AlertController, NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { StaffhistoryPage } from '../../pages/staffhistory/staffhistory';
import { Storage } from '@ionic/storage';
import { ViewController } from 'ionic-angular/navigation/view-controller';
/**
 * Generated class for the StaffpopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'staffpopover',
  templateUrl: 'staffpopover.html'
})
export class StaffpopoverComponent {
  staffhistory: string;
  staff: string;
  staffId: string;
  apartmentId: string;
  reason: string;
  page: string;
  user: string;
  stafflist: string;
  staffRelationId: string;
  isShowOldPic = true;
  isShowCurPic = false;
  currentPhoto: string;
  srId: string;
  staffBrRelId: string;

  constructor(public viewCtrl: ViewController, public storage: Storage, public api: ApiProvider, public alertCtrl: AlertController, public navParams: NavParams, public navctrl: NavController) {
    this.staff = this.navParams.get('item');
    console.log(this.staff);
    this.storage.get('user').then(res => {
      this.user = res;
      
      if (this.staff != undefined) {
        // this.api.history(this.staff['id'], this.staff['apartmentId'], '10000', this.user['id']).then((res) => {
        //   const data = JSON.parse(res['_body']);
        //   if (data != null) {
        //     this.staffhistory = data[0];
        //   }
        // });
      }
    });
}

ionViewWillEnter(){
  this.staff = this.navParams.get('item');
}

onClickAddBreak() {
  let alert = this.alertCtrl.create();
  alert.setTitle('Select Reason');

  alert.addInput({
    type: 'radio',
    label: 'Officework',
    value: 'officework',
    checked: true
  });
  alert.addInput({
    type: 'radio',
    label: 'Tea',
    value: 'Tea'
  });
  alert.addInput({
    type: 'radio',
    label: 'Lunch',
    value: 'lunch'
  });
  alert.addInput({
    type: 'radio',
    label: 'Other',
    value: 'other'
  });

  alert.addButton('Cancel');
  alert.addButton({
    text: 'AddBreak',
    handler: (data) => {
      this.reason = data;
      // this.api.addbreak(this.staff['id'], this.staff['apartmentId'], this.reason, this.staff['rId'], this.user['id']).then((res) => {
      //   this.ionViewWillEnter();
      // });
    }
  });
  alert.present();
  this.viewCtrl.dismiss();
}

onClickStopBreak() {
  // this.api.stopbreak(this.staffhistory['id'], this.staff['id'], this.user['id']).then((res) => {
  //   this.ionViewWillEnter();
  // });
  this.viewCtrl.dismiss();
}

onClickHistory() {
  this.navctrl.push(StaffhistoryPage, { item: this.staff, srId: this.srId });
  this.viewCtrl.dismiss();
  console.log(this.staff,this.srId)
}
}
