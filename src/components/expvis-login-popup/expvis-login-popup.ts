import { Component } from '@angular/core';
import { AlertController, ViewController, ToastController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ExpvisLoginPopupComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'expvis-login-popup',
  templateUrl: 'expvis-login-popup.html'
})
export class ExpvisLoginPopupComponent {

  item = {};
  user = {};
  pin: string = '';
  isPin = false;
  name: string;
  isName = false;
  phoneNo: string;
  isPhone = false;

  constructor(public toastCtrl: ToastController, public viewCtrl: ViewController, public store: Storage, public navParams: NavParams, public alertCtrl: AlertController, public api: ApiProvider,) {
    this.item = this.navParams.get('item');
    this.store.get('user').then(val => {
      this.user = val;
    })
  }

  // onClickAllow() {
  //   if (!this.name) {
  //     return this.isName = true;
  //   } else {
  //     this.isName = false;
  //   }
  //   if (!this.phoneNo) {
  //     return this.isPhone = true;
  //   } else {
  //     this.isPhone = false;
  //   }
  //   //   if (this.pin == this.item['pinNo']) {
  //   //     // this.api.expVisLogin(this.pin, this.phoneNo, this.name, this.user['apartmentId'], this.user['username'], this.item['vehicleNo'], this.item['blockNumber'],this.user['id']);
  //   //     // this.viewCtrl.dismiss();
  //   //     // const toast = this.toastCtrl.create({
  //   //     //   message: 'Verification done.!',
  //   //     //   duration: 1000,
  //   //     //   position: 'bottom',
  //   //     //   cssClass: 'toast-green'
  //   //     });
  //   //     toast.present();
  //   //   }
  //   //   else {
  //   //     this.isPin = true;
  //   //     return;
  //   //   }
  //   // }

  //   // onClickCancel() {
  //   //   this.viewCtrl.dismiss();
  //   // }

  // }
}
